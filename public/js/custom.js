/* Thanks to CSS Tricks for pointing out this bit of jQuery
https://css-tricks.com/equal-height-blocks-in-rows/
It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){

var currentTallest = 0,
    currentRowStart = 0,
    rowDivs = new Array(),
    $el,
    topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}


$(window).on('load', function() { 

  equalheight('#tk .col-md-3.image-thumbnail');
});


$(window).resize(function(){
  equalheight('#tk .col-md-3.image-thumbnail');
});








$(".file-loading").fileinput({
      theme: 'fa',
      previewFileType: "image",
      browseClass: "btn btn-success",
      browseLabel: "Pick Image",
    allowedFileTypes: ["image"],      
      removeClass: "btn btn-danger",
      removeLabel: "Delete",
      uploadClass: "btn btn-info",
      uploadLabel: "Upload",
      browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
      removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i> ",            
      uploadIcon: "<i class=\"glyphicon glyphicon-upload\"></i> ",
      maxFileSize : 2000,
  });

$('#reunion_gallery','#fa_image_url').on('change',function(val, element){



        var size = this.files[0].size;
         console.log(size);

         if (size > 3000000) // checks the file more than 1 MB
         {
            alert('​The image size is exceeding the limit of 2000 kb.')
             this.value = '';
         } 

    });


$(".treeview-menu").has("li.active").parent().addClass('active');




// $('.ui.normal.dropdown').dropdown({
//       maxSelections: 15
//     });


// function show_alert(){

//   swal({
//       title: "Are you sure?",
//       showCancelButton: true,
//       confirmButtonColor: "#dd6f54",
//       confirmButtonText: "Yes",
//       cancelButtonText: "No",
//       closeOnConfirm: true,
//       closeOnCancel: true
//     },
//     function(isConfirm) {
//       if (isConfirm) {
//         return true;
//       } else {
//         return false;
//       }
//     });

// }


