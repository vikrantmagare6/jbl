<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Front End 

    // Reunion

    Route::get('re-union/add-your-picture','StudentController@create');
    Route::post('re-union','StudentController@store');


    // Contact 

    Route::get('/contact-us','ContactController@create');
    Route::post('/contact-us','ContactController@store');

    // About us

    Route::get('/about-us','FrontendController@aboutUs');


    // Events

    Route::get('/events', 'FrontendController@events');
    Route::get('/events/{id}', 'FrontendController@eventsdetail');


    Route::get('/', 'FrontendController@home');













    Route::get('/join-the-association', 'FrontendController@joinTheAssociation');
    Route::get('/what-we-need', 'FrontendController@whatWeNeed');
    Route::get('/connect', 'FrontendController@connect');
    Route::get('/reunion', 'FrontendController@reunion');
    Route::get('/gallery', 'FrontendController@gallery');

    Route::get('thank-you', function () {
        return view('front-end.pages.reunion-thank-you');
    });


    Route::get('404',['as'=> 'notfound','uses' => 'FrontendController@pagenotfound']);









































// Backend 

Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.

    Route::get('/admin', function () {
        return view('admin/home');
    });

    // Home Title

    Route::get('/admin/home', 'OptionController@home');

    // Banner

    Route::resource('/admin/banner-images','BannerController');

    // Events
    Route::resource('/admin/events','EventController');





    // About Us

    Route::get('/admin/options/create','OptionController@create');
    Route::post('/admin/options','OptionController@test');
    Route::post('/admin/options','OptionController@test');
    Route::get('/admin/history', 'OptionController@historyshow');

    Route::post('admin/store', 'OptionController@store');

    Route::get('/admin/principal', 'OptionController@principal');


    Route::get('/admin/join-the-association', 'OptionController@joinTheAssociation');

    Route::get('/admin/what-we-need', 'OptionController@whatWeNeed');

    // Event Gallery
    Route::post('/admin/events/image/{id}', 'EventController@deleteImageById');

    // Re-union Gallery
    Route::post('/admin/re-union/image/{id}', 'ReunionController@deleteImageById');


    // committee 
    Route::resource('/admin/committee', 'CommitteeController');

    // Gallery

    Route::get('/admin/gallery','GalleryController@index');
    Route::get('/admin/gallery/create','GalleryController@create');
    Route::post('/admin/gallery','GalleryController@store');
    Route::delete('/admin/gallery/{id}','GalleryController@delete');


    // Re-Union 

    Route::resource('/admin/re-union','ReunionController');

    Route::get('admin/student-list','StudentController@index');

    Route::get('admin/student-list/{id}','StudentController@show');
    Route::delete('admin/student-list/{id}','StudentController@destroy');


    // Feature Alumni 

    Route::resource('/admin/featured-alumni','FeaturealumniController');


    Route::resource('/admin/merchandise','MerchandiseController');


    Route::resource('/admin/project','ProjectController');

    Route::resource('/admin/alumni-directory','DirectoryController');

    Route::get('/admin/contact','ContactController@index');
    Route::delete('/admin/contact/{id}','ContactController@delete');

    Route::get('/admin/export-contact-list','ContactController@exportContactList');


    



    
    Route::get('/admin/download/image/{id}', 'FrontendController@donwloadImg');

    // Route::get('/admin/download/image', 'FrontendController@donwloadImg2');




    










    
});


    




















