<?php

namespace App\Providers;
use View;
use App\Model\Optionjb;

use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;
use  Illuminate\Support\Facades\Schema; // At the top of your file


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
        $allOption = Optionjb::all();
        View::share('allOption',$allOption);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
