<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Reunion;
use App\Model\Imagesfile;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

class ReunionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reunions = Reunion::orderBy("year")->get();
        return view('admin.pages.re-union.index')->with('reunions',$reunions);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.re-union.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $tempa = '';
        if ($request->hasFile('reunion_thumbnail')) {
            $tempa = 'mimes:jpe,jpeg,jpg,png|max:2000';

        }
        $tempa2 = 'dfsdfsdf';
        if ($request->hasFile('reunion_gallery')) {
            $tempa2 = 'mimes:jpe,jpeg,jpg,png|max:3000';
            
        }

        return $tempa2;
        $messages = [
            
            'reunion_thumbnail.required' => 'Image field is required.',
            'reunion_thumbnail.mimes' => 'jpg and png only',
            'reunion_thumbnail.max' => '​The image size is exceeding the limit of 2000 kb.',
            'reunion_gallery' => 'this i'
            
            
            
            
            
        ];
        $this->validate($request,[
            
            'reunion_title' => 'required|max:120',  
            'year' => 'required|numeric',
            'reunion_thumbnail' => 'mimes:jpe,jpeg,jpg,png|max:2000',
            'reunion_thumbnail' => $tempa,
            'reunion_gallery.*' => $tempa2,

            
            
        ], $messages);

        if ($request->hasFile('reunion_thumbnail')) {

            $reunion_thumbnail = $request->file('reunion_thumbnail');
            $filename = time().'.'.$reunion_thumbnail->getClientOriginalExtension();
            Image::make($reunion_thumbnail)->save(public_path('/uploads/images/'.$filename));
         }else {

            $filename =  'default.png';
        }

        $newReunion = new Reunion;

        $newReunion->reunion_title = $request['reunion_title'];
        $newReunion->reunion_thumbnail = $filename;
        $newReunion->year = $request['year'];
        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }
        $newReunion->is_active = $is_active;

        $newReunion->save();

        if ($request->hasFile('reunion_gallery')){
            if ($request->hasFile('reunion_gallery')) {
                $files = Input::file('reunion_gallery');
                foreach ($files as $file) {
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $name = $timestamp. '-' .$file->getClientOriginalName();
                    $path = public_path().'/uploads/allimg/';
                    $file->move($path, $name);

                    // make a ProductImage Object and save
                    $galleryImage = new Imagesfile();
                    $galleryImage->images_url = $name;
                    // $galleryImage->image_alt = $name;
                    $newReunion->imagesfile()->save($galleryImage);
                    $event_status = "successfully created";
                }
            }else{
                DB::rollBack();
                $event_status = "some error occured at serve";
            }

            DB::commit();


            // return Redirect::back()->withErrors(['fail' => 'Please Select Atleast one image']);
        }

        // return $event_status;

        $reunions = Reunion::all();
        // return view('admin.pages.re-union.index')->with('reunions',$reunions);
        return redirect('/admin/re-union')->with('reunions',$reunions);
        


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reunion =Reunion::findOrFail($id);
        return view('admin.pages.re-union.edit')->with('reunion',$reunion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            
            'reunion_title' => 'required|max:120',  
            'year' => 'required|numeric'
           
            
        ]);
        $reunion = Reunion::findOrFail($id);        
        $reunion->reunion_title = $request->reunion_title;
        $reunion->year = $request->year;

        if ($request->hasFile('reunion_thumbnail')) {

            $reunion_thumbnail = $request->file('reunion_thumbnail');
            $filename = time().'.'.$reunion_thumbnail->getClientOriginalExtension();
            Image::make($reunion_thumbnail)->save(public_path('/uploads/images/'.$filename));


            $reunion->reunion_thumbnail = $filename;

        }

        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }

        


        
                
        
        $reunion->is_active = $is_active;
        $reunion->save();

         if ($request->hasFile('reunion_gallery')){
            if ($request->hasFile('reunion_gallery')) {
                $files = Input::file('reunion_gallery');
                foreach ($files as $file) {
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $name = $timestamp. '-' .$file->getClientOriginalName();
                    $path = public_path().'/uploads/allimg/';
                    $file->move($path, $name);

                    // make a ProductImage Object and save
                    $galleryImage = new Imagesfile();
                    $galleryImage->images_url = $name;
                    // $galleryImage->image_alt = $name;
                    $reunion->imagesfile()->save($galleryImage);
                    $event_status = "successfully created";
                }
            }else{
                DB::rollBack();
                $event_status = "some error occured at serve";
            }

            DB::commit();


            
        }

        $reunions = Reunion::all();
        // return view('admin.pages.re-union.index')->with('reunions',$reunions);
        return redirect('/admin/re-union')->with('reunions',$reunions);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reunion = Reunion::findOrFail($id);

        foreach ($reunion->imagesfile as $img) {
                $fi = Imagesfile::findOrFail($img->id);
                File::delete(public_path().'/uploads/allimg/'.$fi->images_url);

                if ($fi->delete()) {
                    
                }else{
                    DB::rollBack();
                    
                }
            }
        
        $reunion->delete();

        
        $reunions = Reunion::all();
        // return view('admin.pages.re-union.index')->with('reunions',$reunions);
        return redirect('/admin/re-union')->with('reunions',$reunions);
        //
    }

    public function deleteImageById($id) {
        $eventImage = Imagesfile::findOrFail($id);
        $eventImage->delete();
        return response()->json(['successfully']);
    }
}
