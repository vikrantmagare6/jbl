<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Banner;
use Image;
use Carbon\Carbon;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner_data = Banner::all();                
        return view('admin.pages.banner.index')->with('banners',$banner_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.banner.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $messages = [
            'banner_heading.required' => 'The name field is required.',
            'banner_heading.max' => '120 charector only',

            'banner_image.required' => 'Banner image is required',
            'banner_image.mimes' => 'Banner image jpg and png only',
            'banner_image.max' => '​The image size is exceeding the limit of 2000 kb.',

            'banner_description.required' => 'Banner Description required',
            
            'banner_button_text.required' => 'Start Date is Required',
            'banner_button_link.required' => 'End Date is Required',
            
            
            
        ];

        $this->validate($request, [
            'banner_heading' => 'required|max:120',  
            'banner_image' => 'required|mimes:jpeg,jpg,png|max:2000',
            'banner_description' => 'required',
            'banner_button_text' => 'required|max:120',  
            'banner_button_link' => 'required|max:240' 
        ], $messages);
        
   

        if ($request->hasFile('banner_image')) {

            $banner_image = $request->file('banner_image');
            $filename = time().'.'.$banner_image->getClientOriginalExtension();
            Image::make($banner_image)->save(public_path('/uploads/images/'.$filename));
         }else {

            $filename =  'default.png';
        }

        $newBanner = new Banner;

        $newBanner->banner_heading = $request['banner_heading'];
        $newBanner->banner_image = $filename;
        $newBanner->banner_description = $request['banner_description'];
        $newBanner->banner_button_text = $request['banner_button_text'];
        $newBanner->banner_button_link = $request['banner_button_link'];

        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }
        $newBanner->is_active = $is_active;

        $newBanner->save();

        $banner_data = Banner::all();                
        // return view('admin.pages.banner.index')->with('banners',$banner_data);
        return redirect('/admin/banner-images')->with('banners',$banner_data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner =Banner::findOrFail($id);
        return view('admin.pages.banner.edit')->with('banner',$banner);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            
            'banner_heading' => 'required|max:120',  
            'banner_image' => 'mimes:jpeg,jpg,png',
            'banner_button_text' => 'required|max:120',
            'banner_button_link' => 'required|max:240',
            
           
            
        ]);

        if ($request->hasFile('banner_image')) {

            $banner_image = $request->file('banner_image');
            $filename = time().'.'.$banner_image->getClientOriginalExtension();
            Image::make($banner_image)->save(public_path('/uploads/images/'.$filename));


 

            
        }else {
            $filename = $request->banner_e_image;
        }
        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }

        


        
        $banner = Banner::findOrFail($id);
        
        $banner->banner_heading = $request->banner_heading;
        
        $banner->banner_description = $request->banner_description;
        $banner->banner_button_text = $request->banner_button_text;
        $banner->banner_button_link = $request->banner_button_link;
        $banner->banner_image = $filename;
        $banner->is_active = $is_active;


        $banner->save();

        $allBanner = Banner::all();       
        // return view('admin.pages.banner.index',['banners' => $allBanner]);
        return redirect('/admin/banner-images')->with('banners',$allBanner);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);
        
        $banner->delete();

        $allBanner = Banner::all();       
        // return view('admin.pages.banner.index',['banners' => $allBanner]);

        return redirect('/admin/banner-images')->with('banners',$allBanner);

        //
    }
}
