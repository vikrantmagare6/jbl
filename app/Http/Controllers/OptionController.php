<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Optionjb;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Image;

class OptionController extends Controller
{

    public function create()
    {
         return view('admin.pages.option.option');
        //
    }

    public function test(Request $request) {



    	$this->validate($request,[
            
            'option_name' => 'required|unique:optionjbs',  
            
             
            
                
            
        ]);

        $newOption = new Optionjb;

        $newOption->option_name = $request->option_name;

        $newOption->option_value = $request->option_value;

        $newOption->save();

        

        return view('admin.pages.option.option');






    }

    public function historyshow() {
        $about_history_image = Optionjb::where('option_name', 'about_history_image')->first();
        $about_history_content = Optionjb::where('option_name', 'about_history_content')->first();
    	return view('admin/pages/option/about-us-history',['about_history_image' => $about_history_image, 'about_history_content' => $about_history_content]);
    }

    public function store(Request $request) {

        


        $allpost = $request->input();

        // return $request->input()['about_history_content'];



        $tempac = ''; 

        // return $request->input();

        // $allPostCount = count($request->input());


        // return $allPostCount;

        // for( $i = 0; $i <= $allPostCount; $i++ ) {

        // }

        // if ($request->hasFile('about_history_image')) {
        //     return true;
        // }else {
        //     return ;
        // }


        $allImages = $request->file();

        foreach ($allImages as $key => $value) {

             if (Optionjb::where('option_name', $key)->first()) {

                $c_row = Optionjb::where('option_name', $key)->first();

                if ($request->hasFile($key)) {

                    $temp_img = $request->file($key);
                    $temp_img_filename = time().'.'.$temp_img->getClientOriginalExtension();
                    Image::make($temp_img)->save(public_path('/uploads/images/'.$temp_img_filename));
                    
                    $c_row->option_value = $temp_img_filename;
                    $c_row->save();

                    
                 }

                $tempac =  $key;

            }
            
        }

        



        


        

        foreach ($allpost as  $key => $value) { 


             if (Optionjb::where('option_name', $key)->first()) {

                $c_row = Optionjb::where('option_name', $key)->first();





                $c_row->option_value = $request[$key];
                $c_row->save();
                
            }


            

            
        }





        return Redirect::back()->withErrors(['success' => 'Data Saved']);




        // $this->validate($request,[

        //     'option_name' => 'required|unique:optionjbs',  
            
        // ]);

        // $newOption = new Optionjb;

        // $newOption->option_name = $request->option_name;

        // $newOption->option_value = $request->option_value;

        // $newOption->save();

        

        // return view('admin.pages.option.option');
        
    }

    public function principal() {

        
        $about_principal_image = Optionjb::where('option_name', 'about_principal_image')->first();        
        $about_principal_title = Optionjb::where('option_name', 'about_principal_title')->first();        
        $about_principal_subtitle = Optionjb::where('option_name', 'about_principal_subtitle')->first();
        $about_principal_content = Optionjb::where('option_name', 'about_principal_content')->first();
        

        
        return view('admin/pages/option/about-us-principal',['about_principal_image' => $about_principal_image, 'about_principal_title' => $about_principal_title, 'about_principal_subtitle' => $about_principal_subtitle, 'about_principal_content' => $about_principal_content]);
    

    }

    public function home(){

        $home_page_title = Optionjb::where('option_name', 'home_page_title')->first();        
        $home_page_description = Optionjb::where('option_name', 'home_page_description')->first();        
        

        return view('admin/pages/option/home',['home_page_title' => $home_page_title, 'home_page_description' => $home_page_description]);    
    }


    public function joinTheAssociation() {

        $join_the_association = Optionjb::where('option_name', 'join_the_association')->first();        
        $home_page_description = Optionjb::where('option_name', 'home_page_description')->first();        
        

        return view('admin/pages/option/join-the-association',['join_the_association' => $join_the_association, 'home_page_description' => $home_page_description]);




    }

    public function whatWeNeed() {

        $what_we_need = Optionjb::where('option_name', 'what_we_need')->first();        
        
        

        return view('admin/pages/option/what-we-need',['what_we_need' => $what_we_need ]);

    }


    


}
