<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Contact;
use Illuminate\Support\Facades\Redirect;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;


class ContactController extends Controller
{
    
    public function index() {

    	$allContact = Contact::all();
    	return view('admin.pages.contact.index')->with('allContact',$allContact);

    }

    public function create() {
    	return view('front-end.pages.contact');
    }

    public function store(Request $request) {


    	$this->validate($request,[

            'c_name' => 'required|max:120',              
            'c_email_id' => 'required',
            'c_number' => 'required',
            'c_message' => 'required'
            

        ]);

         $newcust = new Contact;

        $newcust->c_name = $request['c_name'];
        $newcust->c_email_id = $request['c_email_id'];                
        $newcust->c_number = $request['c_number'];
        $newcust->c_message = $request['c_message'];
        

        $newcust->save();


        return view('front-end.pages.reunion-thank-you');


    }

    public function delete($id) {

    	$event = Contact::findOrFail($id);
    	$event->delete();    	
    	$allContact = Contact::all();
    	return view('admin.pages.contact.index')->with('allContact',$allContact);


    }

    public function exportContactList() {

        Excel::create('contact-list', function($excel){
            $excel->sheet('contact-list', function($sheet) {

                $contacts = Contact::all();
                $arr_contact = array();

                foreach ($contacts as $contact) {

                    $data = array($contact->c_name, $contact->c_email_id, $contact->c_number, $contact->c_message, $contact->created_at);
                    array_push($arr_contact, $data);
                }

                $sheet->fromArray($arr_contact,null,'A1',false,false)->prependRow(array('Name', 'Email ID', 'Phone Number', 'Message','Created At'));
            });
        })->download('csv');

        

    
    }

}
