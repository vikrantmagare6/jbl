<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Featuredalumni;
use Illuminate\Support\Facades\Redirect;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

class FeaturealumniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $featuredalumnies = Featuredalumni::all();
        return view('admin.pages.featured.index')->with('featuredalumnies',$featuredalumnies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.featured.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'fa_name.required' => 'The name field is required.',
            'fa_designation.required' => 'The designation field is required.',
            'fa_image_url.required' => 'The image url field is required.',
            'fa_image_url.max' => 'Max file size is 1mb',
            'fa_image_url.mimes' => 'jpg and png only',
            'fa_content.required' => 'The content field is required.',
            
        ];

        $this->validate($request, [
            'fa_name' => 'required|max:120',              
            'fa_designation' => 'required',
            'fa_image_url' => 'required|max:10000|mimes:jpe,jpeg,jpg,png',
            'fa_content' => 'required'
        ], $messages);



        

        if ($request->hasFile('fa_image_url')) {

            $fa_image_url = $request->file('fa_image_url');
            $fa_image_url_filename = time().'.'.$fa_image_url->getClientOriginalExtension();
            Image::make($fa_image_url)->save(public_path('/uploads/images/'.$fa_image_url_filename));

         }else {

            $fa_image_url_filename =  'default.png';
        }

        $newFeature = new Featuredalumni;

        $newFeature->fa_name = $request['fa_name'];
        $newFeature->fa_designation = $request['fa_designation'];        
        $newFeature->fa_image_url = $fa_image_url_filename;        
        $newFeature->fa_content = $request['fa_content'];
        

        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }
        $newFeature->is_active = $is_active;

        $newFeature->save();

        $featuredalumnies = Featuredalumni::all();
        // return view('admin.pages.featured.index')->with('featuredalumnies',$featuredalumnies);
        return redirect('/admin/featured-alumni')->with('featuredalumnies',$featuredalumnies);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $featureda =Featuredalumni::findOrFail($id);
        return view('admin.pages.featured.edit')->with('featureda',$featureda);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            
            'fa_name' => 'required|max:120',              
            'fa_designation' => 'required',
            'fa_image_url' => 'max:10000|mimes:jpe,jpeg,jpg',
            'fa_content' => 'required'
            
        ]);

       


        



        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }

        


        
        $update_feature = Featuredalumni::findOrFail($id);        
        $update_feature->fa_name = $request->fa_name;
        $update_feature->fa_designation = $request->fa_designation;
        if ($request->hasFile('fa_image_url')) {

            $fa_image_url = $request->file('fa_image_url');
            $fa_image_url_filename = time().'.'.$fa_image_url->getClientOriginalExtension();
            Image::make($fa_image_url)->save(public_path('/uploads/images/'.$fa_image_url_filename));
            $update_feature->fa_image_url = $fa_image_url_filename;
            
        
        }
        
        $update_feature->fa_content = $request->fa_content;        
        
        $update_feature->is_active = $is_active;


        $update_feature->save();

        $featuredalumnies = Featuredalumni::all();
        return view('admin.pages.featured.index')->with('featuredalumnies',$featuredalumnies);
        return redirect('/admin/featured-alumni')->with('featuredalumnies',$featuredalumnies);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $featureda =Featuredalumni::findOrFail($id);
        $featureda->delete();
        
        $featuredalumnies = Featuredalumni::all();
        return redirect('/admin/featured-alumni')->with('featuredalumnies',$featuredalumnies);
        
    }
}
