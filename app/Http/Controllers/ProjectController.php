<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Model\Project;
use App\Model\Imagesfile;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allProject = Project::all();
        return view('admin.pages.project.index')->with('allProject',$allProject);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $messages = [
            'p_name.required' => 'The name field is required.',
            'p_name.max' => '120 charector only',
            'p_feature_image.max' => 'Max file size 1 mb',
            'p_feature_image.mimes' => 'jpg and pnd only',
            
            'p_start_date.required' => 'Start Date is Required',
            'p_end_date.required' => 'End Date is Required',
            'p_content.required' => 'Content is Required',
            
            
        ];

        $this->validate($request, [
           'p_name' => 'required|max:120',                          
            'p_feature_image' => 'max:10000|mimes:jpe,jpeg,jpg',            
            'p_start_date' => 'required',
            'p_end_date' => 'required',
            'p_content' => 'required'
        ], $messages);

        



        

        if ($request->hasFile('p_feature_image')) {

            $p_feature_image = $request->file('p_feature_image');
            $p_feature_image_filename = time().'.'.$p_feature_image->getClientOriginalExtension();
            Image::make($p_feature_image)->save(public_path('/uploads/images/'.$p_feature_image_filename));

         }else {

            $p_feature_image_filename =  'default.png';
        }

        $newProject = new Project;
        $newProject->p_feature_image = $p_feature_image_filename;   
        $newProject->p_name = $request['p_name'];        
        $newProject->p_content = $request['p_content'];        
        $newProject->p_start_date = Carbon::parse($request['p_start_date']);
        $newProject->p_end_date = Carbon::parse($request['p_end_date']);        
        
        
        

        $is_current = false;

        if ($request['is_current'] === 'true') {
            $is_current = true;
        }else{
            $is_current = false;
        }
        $newProject->is_current = $is_current;
        

        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }
        $newProject->is_active = $is_active;

        $newProject->save();

        if ($request->hasFile('event_gallery')){
            if ($request->hasFile('event_gallery')) {
                $files = Input::file('event_gallery');
                foreach ($files as $file) {
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $name = $timestamp. '-' .$file->getClientOriginalName();
                    $path = public_path().'/uploads/allimg/';
                    $file->move($path, $name);

                    // make a ProductImage Object and save
                    $galleryImage = new Imagesfile();
                    $galleryImage->images_url = $name;
                    // $galleryImage->image_alt = $name;
                    $newProject->imagesfile()->save($galleryImage);
                    $event_status = "successfully created";
                }
            }else{
                DB::rollBack();
                $event_status = "some error occured at serve";
            }

            DB::commit();


            
        }

        $allProject = Project::all();
        // return view('admin.pages.project.index')->with('allProject',$allProject);
        return redirect('/admin/project')->with('allProject',$allProject);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project =Project::findOrFail($id);
        return view('admin.pages.project.edit')->with('project',$project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            
            'p_name' => 'required|max:120',                          
            'p_feature_image' => 'max:10000|mimes:jpe,jpeg,jpg',            
            'p_start_date' => 'required',
            'p_end_date' => 'required',
            'p_content' => 'required'
            
        ]);


        $is_current = false;

        if ($request['is_current'] === 'true') {
            $is_current = true;
        }else{
            $is_current = false;
        }





        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }

        
        $update_Project = Project::findOrFail($id);        
        $update_Project->p_name = $request->p_name;        
        $update_Project->p_content = $request->p_content;    


        

        $update_Project->p_start_date = Carbon::parse($request->p_start_date);        
        $update_Project->p_end_date = Carbon::parse($request->p_end_date);        


        if ($request->hasFile('p_feature_image')) {

            $p_feature_image = $request->file('p_feature_image');
            $p_feature_image_filename = time().'.'.$p_feature_image->getClientOriginalExtension();
            Image::make($p_feature_image)->save(public_path('/uploads/images/'.$p_feature_image_filename));
            $update_Project->p_feature_image = $p_feature_image_filename;
            
        
        }

        
        
        
        
        $update_Project->is_active = $is_active;
        $update_Project->is_current = $is_current;


        $update_Project->save();


        if ($request->hasFile('event_gallery')){
            if ($request->hasFile('event_gallery')) {
                $files = Input::file('event_gallery');
                foreach ($files as $file) {
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $name = $timestamp. '-' .$file->getClientOriginalName();
                    $path = public_path().'/uploads/allimg/';
                    $file->move($path, $name);

                    // make a ProductImage Object and save
                    $galleryImage = new Imagesfile();
                    $galleryImage->images_url = $name;
                    // $galleryImage->image_alt = $name;
                    $update_Project->imagesfile()->save($galleryImage);
                    $event_status = "successfully created";
                }
            }else{
                DB::rollBack();
                $event_status = "some error occured at serve";
            }

            DB::commit();


            
        }



        $allProject = Project::all();
        // return view('admin.pages.project.index')->with('allProject',$allProject);
        return redirect('/admin/project')->with('allProject',$allProject);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project =Project::findOrFail($id);
        $project->delete();
        
        $allProject = Project::all();
        // return view('admin.pages.project.index')->with('allProject',$allProject);
        return redirect('/admin/project')->with('allProject',$allProject);
    }
}
