<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Model\Merchandise;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

class MerchandiseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allMerchandise = Merchandise::all();
        return view('admin.pages.merchandise.index')->with('allMerchandise',$allMerchandise);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.merchandise.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $messages = [
            'm_name.required' => 'The name field is required.',
            'm_name.max' => '120 charector only',
            'm_image1.max' => 'Max file size 1 mb',
            'm_image1.mimes' => 'jpg and png only',
            'm_image2.max' => 'Max file size is 1mb',
            'm_image2.mimes' => 'jpg and png only',
            'm_price.required' => 'Price is required',
            'm_price.integer' => 'Price Number only',
            'm_price.min' => 'Minimum Price is 0',
            
            
        ];

        $this->validate($request, [
            'm_name' => 'required|max:120',                          
            'm_image1' => 'max:10000|mimes:jpe,jpeg,jpg,png',
            'm_image2' => 'max:10000|mimes:jpe,jpeg,jpg,png',
            'm_price' => 'required|integer|min:0'
        ], $messages);


      


        

        if ($request->hasFile('m_image1')) {

            $m_image1 = $request->file('m_image1');
            $m_image1_filename = time().'w.'.$m_image1->getClientOriginalExtension();
            Image::make($m_image1)->save(public_path('/uploads/images/'.$m_image1_filename));

         }else {

            $m_image1_filename =  'default.png';
        }

        if ($request->hasFile('m_image2')) {

            $m_image2 = $request->file('m_image2');
            $m_image2_filename = time().'.'.$m_image2->getClientOriginalExtension();
            Image::make($m_image2)->save(public_path('/uploads/images/'.$m_image2_filename));

         }else {

            $m_image2_filename =  'default.png';
        }

        $newMerchandise = new Merchandise;

        $newMerchandise->m_name = $request['m_name'];        
        $newMerchandise->m_image1 = $m_image1_filename;   
        $newMerchandise->m_image2 = $m_image2_filename;        
        $newMerchandise->m_price = $request['m_price'];
        

        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }
        $newMerchandise->is_active = $is_active;

        $newMerchandise->save();

        $allMerchandise = Merchandise::all();
        // return view('admin.pages.merchandise.index')->with('allMerchandise',$allMerchandise);
        return redirect('/admin/merchandise')->with('allMerchandise',$allMerchandise);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $merchand =Merchandise::findOrFail($id);
        return view('admin.pages.merchandise.edit')->with('merchand',$merchand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            
            'm_name' => 'required|max:120',                          
            'm_image1' => 'max:10000|mimes:jpe,jpeg,jpg,png',
            'm_image2' => 'max:10000|mimes:jpe,jpeg,jpg,png',
            'm_price' => 'required'
            
        ]);

        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }

        
        $update_merchandise = Merchandise::findOrFail($id);        
        $update_merchandise->m_name = $request->m_name;        
        if ($request->hasFile('m_image1')) {

            $m_image1 = $request->file('m_image1');
            $m_image1_filename = time().'w.'.$m_image1->getClientOriginalExtension();
            Image::make($m_image1)->save(public_path('/uploads/images/'.$m_image1_filename));
            $update_merchandise->m_image1 = $m_image1_filename;
            
        
        }else {

            $m_image1_filename =  'default.png';
        }

        if ($request->hasFile('m_image2')) {

            $m_image2 = $request->file('m_image2');
            $m_image2_filename = time().'.'.$m_image2->getClientOriginalExtension();
            Image::make($m_image2)->save(public_path('/uploads/images/'.$m_image2_filename));
            $update_merchandise->m_image2 = $m_image2_filename;
            
        
        }else {

            $m_image2_filename =  'default.png';
        }
        
        $update_merchandise->m_price = $request->m_price;        
        
        $update_merchandise->is_active = $is_active;


        $update_merchandise->save();

        $allMerchandise = Merchandise::all();
        // return view('admin.pages.merchandise.index')->with('allMerchandise',$allMerchandise);
        return redirect('/admin/merchandise')->with('allMerchandise',$allMerchandise);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $merchand =Merchandise::findOrFail($id);
        $merchand->delete();
        
        $allMerchandise = Merchandise::all();
        // return view('admin.pages.merchandise.index')->with('allMerchandise',$allMerchandise);
        return redirect('/admin/merchandise')->with('allMerchandise',$allMerchandise);
    }
}
