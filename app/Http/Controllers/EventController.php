<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Event;
use App\Model\Imagesfile;
use Illuminate\Support\Facades\Redirect;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;


class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $events = Event::all();                
        return view('admin.pages.event.index')->with('events',$events);
        // return 'hi';
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.pages.event.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $messages = [
            'event_name.required' => 'The name field is required.',
            'event_large_img.required' => 'Image field is required.',
            'event_large_img.mimes' => 'jpg and png only',
            'event_large_img.max' => '​The image size is exceeding the limit of 5000 kb.',
            'event_date_time.required' => 'Event Date time is required',
            'event_gallery.max' => '​The image size is exceeding the limit of 5000 kb.',
            'event_gallery.mimes' => 'Event Gallery jpg and png only'

            
            
            
        ];

        $this->validate($request, [
            'event_name' => 'required|max:120',
            'event_large_img' => 'required|mimes:jpe,jpeg,jpg,png|max:5000',
            'event_date_time' => 'required',
            'event_gallery.*' => 'mimes:jpe,jpeg,jpg,png|max:2000'
        ], $messages);

       


        



        

        if ($request->hasFile('event_large_img')) {

            $event_large_img = $request->file('event_large_img');
            $event_large_img_filename = time().'.'.$event_large_img->getClientOriginalExtension();
            Image::make($event_large_img)->save(public_path('/uploads/images/'.$event_large_img_filename));
         }else {

            $event_large_img_filename =  'default.png';
        }

        $newEvent = new Event;

        $newEvent->event_name = $request['event_name'];
        $newEvent->event_short_description = $request['event_short_description'];        
        $newEvent->event_large_img = $event_large_img_filename;        
        $newEvent->event_content = $request['event_content'];
        $newEvent->event_date_time = Carbon::parse($request['event_date_time'].$request['timepicker']);

        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }
        $newEvent->is_active = $is_active;

        $newEvent->save();

         if ($request->hasFile('event_gallery')){
            if ($request->hasFile('event_gallery')) {
                $files = Input::file('event_gallery');
                foreach ($files as $file) {
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $name = $timestamp. '-' .$file->getClientOriginalName();
                    $path = public_path().'/uploads/allimg/';
                    $file->move($path, $name);

                    // make a ProductImage Object and save
                    $galleryImage = new Imagesfile();
                    $galleryImage->images_url = $name;
                    // $galleryImage->image_alt = $name;
                    $newEvent->imagesfile()->save($galleryImage);
                    $event_status = "successfully created";
                }
            }else{
                DB::rollBack();
                $event_status = "some error occured at serve";
            }

            DB::commit();


            // return Redirect::back()->withErrors(['fail' => 'Please Select Atleast one image']);
        }

        

        // return $event_status;

        $events = Event::all();                
        // return view('admin.pages.event.index')->with('events',$events);
        return redirect('/admin/events')->with('events',$events);

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $event =Event::findOrFail($id);
        return view('admin.pages.event.show')->with('event',$event);

        



        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event =Event::findOrFail($id);
        return view('admin.pages.event.edit')->with('event',$event);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            
            'event_name' => 'required|max:120', 
            
            'event_large_img' => 'mimes:jpe,jpeg,jpg,png',
          
            'event_date_time' => 'required'   
           
            
        ]);

        // if ($request->hasFile('event_thumbnail')) {

        //     $event_thumbnail = $request->file('event_thumbnail');
        //     $event_thumbnail_filename = time().'.'.$event_thumbnail->getClientOriginalExtension();
        //     Image::make($event_thumbnail)->save(public_path('/uploads/images/'.$event_thumbnail_filename));
            
        // }else {
        //     $event_thumbnail_filename = $request->event_e_thumbnail;
        // }


        if ($request->hasFile('event_large_img')) {

            $event_large_img = $request->file('event_large_img');
            $event_large_img_filename = time().'.'.$event_large_img->getClientOriginalExtension();
            Image::make($event_large_img)->save(public_path('/uploads/images/'.$event_large_img_filename));
            
        }else {
            $event_large_img_filename = $request->event_e_large_img;
        }



        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }

        


        
        $update_event = Event::findOrFail($id);        
        $update_event->event_name = $request->event_name;
        $update_event->event_short_description = $request->event_short_description;
        // $update_event->event_thumbnail = $event_thumbnail_filename;
        $update_event->event_large_img = $event_large_img_filename;
        $update_event->event_content = $request->event_content;        
        $update_event->event_date_time =  Carbon::parse($request->event_date_time.$request->timepicker);
        $update_event->is_active = $is_active;


        $update_event->save();

        if ($request->hasFile('event_gallery')){
            if ($request->hasFile('event_gallery')) {
                $files = Input::file('event_gallery');
                foreach ($files as $file) {
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $name = $timestamp. '-' .$file->getClientOriginalName();
                    $path = public_path().'/uploads/allimg/';
                    $file->move($path, $name);

                    // make a ProductImage Object and save
                    $galleryImage = new Imagesfile();
                    $galleryImage->images_url = $name;
                    // $galleryImage->image_alt = $name;
                    $update_event->imagesfile()->save($galleryImage);
                    $event_status = "successfully created";
                }
            }else{
                DB::rollBack();
                $event_status = "some error occured at serve";
            }

            DB::commit();


            
        }



        $events = Event::all();                
        // return view('admin.pages.event.index')->with('events',$events);
        return redirect('/admin/events')->with('events',$events);

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        
        $event = Event::findOrFail($id);

        foreach ($event->imagesfile as $img) {
                $fi = Imagesfile::findOrFail($img->id);
                File::delete(public_path().'/uploads/allimg/'.$fi->images_url);

                if ($fi->delete()) {
                    
                }else{
                    DB::rollBack();
                    
                }
            }
        
        $event->delete();

        $events = Event::all();                
        // return view('admin.pages.event.index')->with('events',$events);
        return redirect('/admin/events')->with('events',$events);
        //
    }

    public function deleteImageById($id) {
        $eventImage = Imagesfile::findOrFail($id);
        $eventImage->delete();
        return response()->json(['successfully']);
    }
}
