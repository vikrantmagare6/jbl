<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Banner;
use App\Model\Gallery;
use Image;
use App\Model\Committee;
use Carbon\Carbon;

class GalleryController extends Controller
{
    public function index() {

    	$gallery = Gallery::all();
    	return view('admin.pages.gallery.index')->with('gallery',$gallery);



    }

    public function create() {
    	return view('admin.pages.gallery.create');
    }

    public function store(Request $request) {


    	$this->validate($request,[            
            
            'gallery_image' => 'max:1000|mimes:jpe,jpeg,jpg,png',      
            
        ]);

        if ($request->hasFile('gallery_image')) {

            $gallery_image = $request->file('gallery_image');
            $gallery_filename = time().'.'.$gallery_image->getClientOriginalExtension();
            Image::make($gallery_image)->save(public_path('/uploads/images/'.$gallery_filename));
         }else {

            $gallery_filename =  'default.png';
        }

        $newImage = new Gallery;

        $newImage->gallery_caption = $request['gallery_caption'];
        $newImage->gallery_image = $gallery_filename;
        
        $newImage->save();

        $gallery = Gallery::all();
    	// return view('admin.pages.gallery.index')->with('gallery',$gallery);
        return redirect('/admin/gallery')->with('gallery',$gallery);
        
    }

    public function delete($id) {

        $gal = Gallery::findOrFail($id);
        
        $gal->delete();

        $gallery = Gallery::all();
        // return view('admin.pages.gallery.index')->with('gallery',$gallery);
        return redirect('/admin/gallery')->with('gallery',$gallery);

    }


}
