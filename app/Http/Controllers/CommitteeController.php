<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Model\Banner;
use Image;
use App\Model\Committee;
use Carbon\Carbon;


class CommitteeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $committees = Committee::all();                
        return view('admin.pages.committee.index')->with('committees',$committees);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.committee.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            
            'committee_name' => 'required|max:120',  
            'committee_designation' => 'required',         
            'committee_description' => 'required',        
            'committee_category' => 'required',
            
        ]);

        if ($request->hasFile('committee_img')) {

            $committee_img = $request->file('committee_img');
            $committee_img_filename = time().'.'.$committee_img->getClientOriginalExtension();
            Image::make($committee_img)->save(public_path('/uploads/images/'.$committee_img_filename));
         }else {

            $committee_img_filename =  'default.png';
        }

        $newMember = new Committee;

        $newMember->committee_name = $request['committee_name'];
        $newMember->committee_img = $committee_img_filename;
        $newMember->committee_designation = $request['committee_designation'];
        $newMember->committee_description = $request['committee_description'];
        $newMember->committee_category = $request['committee_category'];

        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }
        $newMember->is_active = $is_active;

        $newMember->save();

        $committees = Committee::all();                
        // return view('admin.pages.committee.index')->with('committees',$committees);
        return redirect('/admin/committee')->with('committees',$committees);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $committee = Committee::findOrFail($id);
        return view('admin.pages.committee.edit')->with('committee',$committee);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            
            'committee_name' => 'required|max:120',  
            'committee_designation' => 'required',  
            'committee_description' => 'required',
            'committee_category' => 'required',
            'committee_img' => 'max:10000|mimes:jpe,jpeg,jpg',
            
            
 
           
            
        ]);

        
        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }

        


        
        $committee = Committee::findOrFail($id);
        
        $committee->committee_name = $request->committee_name;
        
        $committee->committee_designation = $request->committee_designation;
        $committee->committee_description = $request->committee_description;
        $committee->committee_category = $request->committee_category;
        
        $committee->is_active = $is_active;

        if ($request->hasFile('committee_img')) {

            $committee_img = $request->file('committee_img');
            $committee_img_filename = time().'.'.$committee_img->getClientOriginalExtension();
            Image::make($committee_img)->save(public_path('/uploads/images/'.$committee_img_filename));

            $committee->committee_img = $committee_img_filename;


 

        
        }


        $committee->save();

        $committees = Committee::all();
        // return view('admin.pages.committee.index')->with('committees',$committees);
        return redirect('/admin/committee')->with('committees',$committees);


        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

         $committee = Committee::findOrFail($id);
        
        $committee->delete();

        $committees = Committee::all();                
        // return view('admin.pages.committee.index')->with('committees',$committees);
        return redirect('/admin/committee')->with('committees',$committees);
        //
    }
}
