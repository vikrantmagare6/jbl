<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Model\Directory;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;


class DirectoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allDirectory = Directory::orderBy('a_year','desc')->get();
        return view('admin.pages.directory.test')->with('allDirectory',$allDirectory);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.directory.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $messages = [
            'a_name.required' => 'The name field is required.',
            
            
            'a_year.max' => 'The year 4 Digit only.',
            'a_year.integer' => 'The number only.',
            'a_cat.required' => 'The category field is required.',
            
        ];
        $tempa = '';
        if ($request['a_year']) {
            $tempa = 'integer|max:9999';

        }

        $this->validate($request, [
            'a_name' => 'required|max:120',                          
            
            'a_year' => $tempa,                          
            'a_cat' => 'required',   
        ], $messages);
        


        $newDirectory = new Directory;
        $newDirectory->a_name = $request['a_name'];   
        $newDirectory->a_email = $request['a_email'];   
        if ($request['a_year']) {
                $newDirectory->a_year = $request['a_year']; 
             }else{
                $newDirectory->a_year = 0; 
             }     
               
        $newDirectory->a_cat = $request['a_cat'];        
        
        
        
        

        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }
        $newDirectory->is_active = $is_active;

        $newDirectory->save();

        
        $allDirectory = Directory::all();
        // return view('admin.pages.directory.index')->with('allDirectory',$allDirectory);
        return redirect('/admin/alumni-directory')->with('allDirectory',$allDirectory);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Directory::findOrFail($id);
        return view('admin.pages.directory.edit')->with('member',$member);       

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            
            'a_name' => 'required|max:120',                          
            'a_email' => 'required',                          
            'a_year' => 'required',                          
            'a_cat' => 'required',                          
            
        ]);


        
        $updateDirectory = Directory::findOrFail($id);        
        $updateDirectory->a_name = $request->a_name;        
        $updateDirectory->a_email = $request->a_email;    
        $updateDirectory->a_year = $request->a_year;    
        $updateDirectory->a_cat = $request->a_cat;     

        $is_active = false;

        if ($request['is_active'] === 'true') {
            $is_active = true;
        }else{
            $is_active = false;
        }
        $updateDirectory->is_active = $is_active;



        $updateDirectory->save();
        $allDirectory = Directory::all();
        // return view('admin.pages.directory.index')->with('allDirectory',$allDirectory);
        return redirect('/admin/alumni-directory')->with('allDirectory',$allDirectory);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Directory::findOrFail($id);
        $member->delete();
        $allDirectory = Directory::all();
        // return view('admin.pages.directory.index')->with('allDirectory',$allDirectory);
        return redirect('/admin/alumni-directory')->with('allDirectory',$allDirectory);
    }
}
