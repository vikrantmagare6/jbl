<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Reunion;
use App\Model\Imagesfile;
use App\Model\Student;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        return view('admin.pages.student.index')->with('students',$students);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('front-end.pages.reunion-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            
            'std_name' => 'required|max:120',  
            
            
        ]);

       

        $newStd = new Student;

        $newStd->std_name = $request['std_name'];
        $newStd->std_email = $request['std_email'];
        $newStd->std_year_of_pass = $request['std_year_of_pass'];

        $newStd->save();

        if ($request->hasFile('std_gallery')){
            if ($request->hasFile('std_gallery')) {
                $files = Input::file('std_gallery');
                foreach ($files as $file) {
                    $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                    $name = $timestamp. '-' .$file->getClientOriginalName();
                    $path = public_path().'/uploads/allimg/';
                    $file->move($path, $name);

                    // make a ProductImage Object and save
                    $galleryImage = new Imagesfile();
                    $galleryImage->images_url = $name;
                    // $galleryImage->image_alt = $name;
                    $newStd->imagesfile()->save($galleryImage);
                    $event_status = "successfully created";
                }
            }else{
                DB::rollBack();
                $event_status = "some error occured at serve";
            }

            DB::commit();


            // return Redirect::back()->withErrors(['fail' => 'Please Select Atleast one image']);
        }

        // return $event_status;

        
        return view('front-end.pages.reunion-thank-you');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::findOrFail($id);
        return view('admin.pages.student.show')->with('student',$student);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::findOrFail($id);

        foreach ($student->imagesfile as $img) {
                $fi = Imagesfile::findOrFail($img->id);
                File::delete(public_path().'/uploads/allimg/'.$fi->images_url);

                if ($fi->delete()) {
                    
                }else{
                    DB::rollBack();
                    
                }
            }
        
        $student->delete();

        $students = Student::all();
        return redirect('/admin/student-list')->with('students',$students);
        
    }
}
