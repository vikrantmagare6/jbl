<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Imagesfile;
use App\Model\Student;
use App\Model\Banner;
use App\Model\Optionjb;
use App\Model\Event;
use App\Model\Committee;
use App\Model\Gallery;
use App\Model\Reunion;
use App\Model\Merchandise;
use App\Model\Project;
use App\Model\Featuredalumni;
use App\Model\Directory;
use Carbon\Carbon;




class FrontendController extends Controller
{
    public function donwloadImg($id){





    	$eventImage = Imagesfile::findOrFail($id);


    	// return Response::download(public_path('/uploads/allimg/').$eventImage->images_url);

    	// return js($eventImage);
    	// return response()->json($eventImage);



    	// return 'dfasdf';

    	// return response()->download('http://localhost/jbl/public/uploads/images/1499758054.jpg');

	    // // 	return $id;

    	return response()->download(public_path('/uploads/allimg/').$eventImage->images_url);
    	 // $filepath = public_path('uploads/image/')."abc.jpg";
    	

    }

    public function aboutUs() {

        $corecommittee = Committee::where('committee_category','core')->get();
        $foundercommittee = Committee::where('committee_category','founding')->get();
        $advisorycommittee = Committee::where('committee_category','advisery')->get();



        $about_history_content = Optionjb::where('option_name', 'about_history_content')->first();
        $about_history_image = Optionjb::where('option_name', 'about_history_image')->first();

        $about_principal_content = Optionjb::where('option_name', 'about_principal_content')->first();
        $about_principal_image = Optionjb::where('option_name', 'about_principal_image')->first();
        $about_principal_title = Optionjb::where('option_name', 'about_principal_title')->first();
        $about_principal_subtitle = Optionjb::where('option_name', 'about_principal_subtitle')->first();

        return view('front-end.pages.about-us',['about_history_content' => $about_history_content, 'about_history_image' => $about_history_image, 'about_principal_subtitle' => $about_principal_subtitle, 'about_principal_title' => $about_principal_title, 'about_principal_image' => $about_principal_image, 'about_principal_content' => $about_principal_content, 'corecommittee'=> $corecommittee, 'foundercommittee' => $foundercommittee, 'advisorycommittee' => $advisorycommittee ]);

    }

    public function events() {

        $upcoming_event = Event::where('event_date_time','>',Carbon::now())->get();
        $past_event = Event::where('event_date_time','<',Carbon::now())->get();
        return view('front-end.pages.events', ['upcoming_event' => $upcoming_event, 'past_event' => $past_event]);
    }

    public function eventsdetail($id) {

        $event =Event::findOrFail($id);
        return view('front-end.pages.event-inner')->with('event',$event);



    }

    public function joinTheAssociation() {

        $join_the_association = Optionjb::where('option_name', 'join_the_association')->first();
        return view('front-end.pages.join-the-association')->with('join_the_association',$join_the_association);
    }

    public function whatWeNeed() {

        
        $what_we_need = Optionjb::where('option_name', 'what_we_need')->first();
        return view('front-end.pages.what-we-need')->with('what_we_need',$what_we_need);
    }

    public function connect() {


        $featuredalumni = Featuredalumni::all();
        $directory = Directory::all();        
        $merchandise = Merchandise::all();
        $project = Project::where('is_current',1)->get();
        $project_current = Project::where('is_current',0)->get();

        $founder = Directory::where('a_cat','founder')->orderBy('a_year','desc')->get();
        $teacher = Directory::where('a_cat','teacher')->orderBy('a_year','desc')->get();
        $normal = Directory::where('a_cat','normal')->orderBy('a_year','desc')->get();
        $silver = Directory::where('a_cat','silver')->orderBy('a_year','desc')->get();
        $honorary = Directory::where('a_cat','honorary')->orderBy('a_year','desc')->get();

        

        return view('front-end.pages.connect',['featuredalumni' => $featuredalumni, 'directory' => $directory, 'project' => $project, 'merchandise' => $merchandise, 'project_current' => $project_current, 'founder' => $founder, 'teacher' => $teacher, 'normal' => $normal, 'silver' => $silver, 'honorary' => $honorary ]);
    }

    public function reunion() {

        $reunion = Reunion::orderBy("year","desc")->get();
        return view('front-end.pages.reunion',['reunion' => $reunion]);
    }

    public function gallery() {

        $gallery = Gallery::all();


        // return $gallery;


        return view('front-end.pages.gallery',['imggal' => $gallery]);




        
    }

    public function pagenotfound(){
        return view('front-end.pages.404');
    }

    public function home() {

        $banner_data = Banner::where('is_active', 1)->get();
        $home_page_title = Optionjb::where('option_name', 'home_page_title')->first();
        $home_page_description = Optionjb::where('option_name', 'home_page_description')->first();
        $upcomingEvent = Event::where('event_date_time','<',Carbon::now())->orderBy('event_date_time','desc')->first();

        // App\Model\Event::where('event_date_time','>',Carbon\Carbon::now())->get()->first();
        
        

        return view('front-end.pages.home',['home_page_title' => $home_page_title, 'home_page_description' => $home_page_description,'slider'=> $banner_data,'upcomingEvent' => $upcomingEvent]);

    }

}
