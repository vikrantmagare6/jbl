<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function imagesfile() {

		return $this->morphMany('App\Model\Imagesfile','imagetable');

	}
}
