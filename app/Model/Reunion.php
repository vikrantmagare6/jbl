<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Reunion extends Model
{
    public function imagesfile() {

		return $this->morphMany('App\Model\Imagesfile','imagetable');

	}
}
