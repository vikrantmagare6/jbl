@extends('admin.layouts.app')


@section('htmlheader_title')
	Home
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<!-- <h3 class="box-title">Home</h3> -->

					</div>
					
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection


@section('page_js')

<script type="text/javascript">
	
	window.location.replace("admin/banner-images");
</script>
@endsection