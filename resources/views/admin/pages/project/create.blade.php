@extends('admin.layouts.app')


@section('htmlheader_title')
  Project
@endsection

@section('page_css')

@endsection

@section('contentheader_title')
Project 
@endsection

@section('contentheader_description')
Create
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Project </li>
    </ol>
@endsection



@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <form enctype="multipart/form-data" method="POST" action="{{ url('admin/project') }}">
              {!! csrf_field() !!}
              <div class="col-md-12">

              <div class="row">
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="p_name" placeholder="Name">
                </div> 

                <div class="col-md-6">                 
                  <input type="text" id="p_start_date" name="p_start_date" class="form-control margin-bottom-30 datepicker"  placeholder="Start Date">
                </div> 
                
              </div>
                
              </div>

              <div class="col-md-6">                 
                  <input type="text" id="p_end_date" name="p_end_date" class="form-control margin-bottom-30 datepicker"  placeholder="End  Date">
              </div> 

              <div class="col-md-6 margin-bottom-30">
                  <label for="p_feature_image" style="display: inline;">Image</label>
                  <input style="    display: inline-block;    margin-left: 20px;" type="file" id="p_feature_image" name="p_feature_image">
                  
              </div>


              <div class="col-md-12 margin-bottom-30">
                
                
                  <textarea id="p_content" class="form-control" name="p_content" rows="10" cols="80"></textarea>
                
              </div>



                


                
                <div class="col-md-6">                  
                  <input type="checkbox" name="is_active" class="minimal" value="true">  Active
                </div> 
                <div class="col-md-6">                  
                  <input type="checkbox" name="is_current" class="minimal" value="true">  Current
                </div> 
                <div class="col-md-12 margin-top-30">
                    <!-- <label class="control-label">Gallery</label> -->
                    <input id="event_gallery" name="event_gallery[]" type="file" multiple class="file-loading">
                </div>
                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Submit">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection


@section('page_js')


<script type="text/javascript">
$(function () {
        CKEDITOR.replace('p_content');
    });
  

  $('.datepicker').datepicker({
      autoclose: true
    });

  var p_start_date =  $('#p_start_date').val();
  var p_end_date = $('#p_end_date').val();



  

  $('#p_end_date').on('blur',function(){

    temp1 = new Date(p_start_date);
    temp2 = new Date(p_end_date);
      console.log(temp1);

      if(new Date(p_start_date) <= new Date(p_end_date)){
          alert('Vikrant is Greate');
        }   
  });
  



</script>





@endsection