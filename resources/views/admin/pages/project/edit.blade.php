@extends('admin.layouts.app')


@section('htmlheader_title')
  Project - Edit
@endsection


@section('contentheader_title')
Project
@endsection

@section('contentheader_description')
Edit
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Project </li>
    </ol>
@endsection

@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <?php //var_dump($banner) ?>
              <form enctype="multipart/form-data" method="POST" action="{{url('/admin/project')}}/{{$project->id}}">
              {{ method_field('PUT') }}
              {!! csrf_field() !!}
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="p_name" placeholder="Name" value=" {!! $project->p_name !!}">
                </div> 

                <div class="col-md-12 margin-bottom-30">
                 <img src="{{ url('/public/uploads/images/')}}/{!! $project->p_feature_image !!}" class="img-responsive margin-bottom-30">
                
                
                  <label for="m_image1" style="display: inline;">Upload Image</label>
                  <input style="    display: inline-block;    margin-left: 20px;" type="file" id="p_feature_image" name="p_feature_image">
                  
                  
                </div> 
                

                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30 datepicker" name="p_start_date" placeholder=" Start Date" value="{!! $project->p_start_date !!}">
                </div> 
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30 datepicker" name="p_end_date" placeholder="End Date" value="{!! $project->p_end_date !!}">
                </div> 

                <div class="col-md-12 margin-bottom-30">
                <label for="p_content">Content</label>
                
                  <textarea id="p_content" class="form-control" name="p_content" rows="10" cols="80">{{ $project->p_content}}</textarea>
                
              </div>
                <div class="col-md-6 margin-top-30">                  
                  <input type="checkbox" name="is_active" class="minimal" value="true" {{$project->is_active == 1 ? 'checked' :''}}> Active
                </div> 

                <div class="col-md-6 margin-top-30">                  
                  <input type="checkbox" name="is_current" class="minimal" value="true" {{$project->is_current == 1 ? 'checked' :''}}> Current
                </div> 

                <div class="col-md-12 margin-top-30 margin-bottom-30">
                     <label class="control-label">Add More Images</label> 
                    <input id="event_gallery" name="event_gallery[]" type="file" multiple class="file-loading">
                </div>

                <div class="col-md-12">

                <div class="row" id="tk" data-token="{{csrf_token()}}">
                     @if(isset($project) && count($project->imagesfile()) > 0)

                     @foreach($project->imagesfile as $img )
                          <div class="col-md-3 image-thumbnail">
                          <button type="button" class="close" onclick="deleteImage({{$img->id}}, this)"><span>X</span></button>
                          <img class="img-thumbnail img-responsive" src="{{ URL::asset('public/uploads/allimg/'.$img->images_url) }}"/>
                          </div>
                     @endforeach

                     @else
                      <div class="col-md-6">
                          <label class="control-label">There are no product images</label>
                      </div>
                     @endif
                  </div>
                  
                </div> 
                  


                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Update">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection


@section('page_js')


<script type="text/javascript">

$(function () {
        CKEDITOR.replace('p_content');
    });


  $('.datepicker').datepicker({
      autoclose: true
    });


  
var token = $("#tk").attr('data-token')
var data = {_token: token};

function deleteImage(id, elem){
  $.ajax({
      url: '{{ URL::to("/admin/events/image")}}/'+id,
      data: data,
      type: 'post',
      success: function (response) {
          // alert('boom');
          $(elem).parent().remove();
          console.log(response);
      }
  });

}







  
</script>








@endsection