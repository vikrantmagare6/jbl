@extends('admin.layouts.app')


@section('htmlheader_title')
  Events
@endsection


@section('contentheader_title')
Events
@endsection

@section('contentheader_description')
Edit
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Events </li>
    </ol>
@endsection

@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <?php //var_dump($banner) ?>
              <form enctype="multipart/form-data" method="POST" action="{{url('/admin/events')}}/{{$event->id}}">
              {{ method_field('PUT') }}
              {!! csrf_field() !!}
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="event_name" placeholder="Event Name" value=" {!! $event->event_name !!}">
                </div> 
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="event_short_description" placeholder="Event Short Description" value="{!! $event->event_short_description !!}">
                </div> 

                 


                <div class="col-md-12">
                 <img src="{{ url('/public/uploads/images/')}}/{!! $event->event_large_img !!}" class="img-responsive margin-bottom-30">
                
                
                  <label for="exampleInputFile" style="display: inline;">Upload Image</label>
                  <input style="    display: inline-block;    margin-left: 20px;" type="file" id="event_large_img" name="event_large_img">
                  <input type="hidden" name="event_e_large_img" value="{!! $event->event_large_img !!}">
                  
                </div> 

                <div class="col-md-12 margin-top-30">
                  <textarea id="event_content" class="form-control" name="event_content" rows="10" cols="80">{{$event->event_content}}</textarea>
                </div>


                <?php 
                $s = $event->event_date_time;
                $dt = new DateTime($s);

                $date = $dt->format('m/d/Y');
                $time = $dt->format('H:i:s');


                 ?>




                <div class="col-md-6 margin-top-30">
                  <input type="text" class="form-control  datepicker" name="event_date_time" placeholder="Event Date" value="<?php echo "$date";  ?> ">
                </div>
                <div class="col-md-6  margin-top-30">
                  
                  <div class="bootstrap-timepicker">
                    <div class="">
                      

                      <div class="input-group">
                        <input type="text" class="  form-control timepicker" name="timepicker" value="<?php echo $time; ?>">

                        <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                      </div>
                      <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                  </div>

                </div>
                <div class="col-md-6 margin-top-30">                  
                  <input type="checkbox" name="is_active" class="minimal" value="true" {{$event->is_active == 1 ? 'checked' :''}}> Active
                </div> 

                <div class="col-md-12 margin-top-30 margin-bottom-30">
                     <label class="control-label">Add More Images</label> 
                    <input id="event_gallery" name="event_gallery[]" type="file" multiple class="file-loading">
                </div>


                <div class="col-md-12">

                <div class="row" id="tk" data-token="{{csrf_token()}}">
                     @if(isset($event) && count($event->imagesfile()) > 0)

                     @foreach( $event->imagesfile as $img )
                          <div class="col-md-3 image-thumbnail">
                          <button type="button" class="close" onclick="deleteImage({{$img->id}}, this)"><span>X</span></button>
                          <img class="img-thumbnail img-responsive" src="{{ URL::asset('public/uploads/allimg/'.$img->images_url) }}"/>
                          </div>
                     @endforeach

                     @else
                      <div class="col-md-6">
                          <label class="control-label">There are no product images</label>
                      </div>
                     @endif
                  </div>
                  
                </div> 
                  


                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Update">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection


@section('page_js')


<script type="text/javascript">
$(function () {
        CKEDITOR.replace('event_content');
    });

$('.datepicker').datepicker({
      autoclose: true
    });


var token = $("#tk").attr('data-token')
var data = {_token: token};

function deleteImage(id, elem){
  $.ajax({
      url: '{{ URL::to("/admin/events/image")}}/'+id,
      data: data,
      type: 'post',
      success: function (response) {
          // alert('boom');
          $(elem).parent().remove();
          console.log(response);
      }
  });

}
      
//Timepicker
$('.timepicker').timepicker({
  showInputs: false,
  showMeridian: false

})
  

  
</script>





@endsection