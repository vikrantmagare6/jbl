@extends('admin.layouts.app')


@section('htmlheader_title')
  Events - Create
@endsection

@section('page_css')

@endsection

@section('contentheader_title')
Events
@endsection

@section('contentheader_description')
Create
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Events </li>
    </ol>
@endsection



@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <form enctype="multipart/form-data" method="POST" action="{{ url('admin/events') }}">
              {!! csrf_field() !!}
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="event_name" placeholder="Event Name">
                </div> 
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="event_short_description" placeholder="Event Description">
                </div> 

                

                <div class="col-md-6 margin-bottom-30">
                  <label for="event_large_img" style="display: inline;">Image</label>
                  <input style="    display: inline-block;    margin-left: 20px;" type="file" id="event_large_img" name="event_large_img">
                  <p class="help-block"></p>
                </div>



                 <div class="col-md-12 margin-bottom-30">                 
                  
                  
                  <textarea id="event_content" class="form-control" name="event_content" rows="10" cols="80"></textarea>
                </div> 
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30 datepicker" name="event_date_time" placeholder="Event Date">
                </div> 
                
                <div class="col-md-6 margin-bottom-30">
                  
                  <div class="bootstrap-timepicker">
                    <div class="">
                      

                      <div class="input-group">
                        <input type="text" class="  form-control timepicker" name="timepicker">

                        <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                      </div>
                      <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                  </div>

                </div>

                <div class="col-md-6 margin-bottom-20">
                  
                  <input type="checkbox" name="is_active" class="minimal" value="true"> Active
                </div> 

                
                  


                
                <div class="col-md-12">
                    <label class="control-label">Gallery</label>
                    <input id="event_gallery" name="event_gallery[]" type="file" multiple class="file-loading">
                </div>
                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Submit">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection


@section('page_js')


<script type="text/javascript">
$(function () {
        CKEDITOR.replace('event_content');
    });

$('.datepicker').datepicker({
      autoclose: true
    });


  
      
//Timepicker
$('.timepicker').timepicker({
  showInputs: false
})
  
  
</script>





@endsection