@extends('admin.layouts.app')


@section('htmlheader_title')
  Events
@endsection


@section('contentheader_title')
Events
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Events </li>
    </ol>
@endsection

@section('main-content')



  <div class="container-fluid spark-screen">
    <div class="row">
      <div class="">




        <!-- Default box -->
        <div class="box">
        
         
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0;">
          <div class="col-xs-12 margin-bottom-10 margin-top-20"> <a class="btn  btn-primary btn-lg" href="events/create">Add</a> </div>
          <div class="col-xs-12">

          <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Event Name</th>
                  <th>Event Description</th>                  
                  <th>Event Tumbnail</th>
                  <th>Event Date</th>
                  <th>Is Active</th>
                  <th>Edit</th>
                  <th>Delete</th>
       
                
                  
                </tr>
                </thead>
                <tbody>
                
                  @forelse($events as $event)
                    <tr>
                      <td><a href="{{url('/admin/events')}}/{{ $event->id }}">{!! $event->event_name !!}</a></td>
                      <td>{!! $event->event_short_description !!}</td>   
                      <td> <img width="200"  src="{{url('/public/uploads/images/')}}/{!! $event->event_large_img !!}" alt="Banner Image"> </td>   
                      <td>{!! $event->event_date_time !!}</td>            
                      <td>   <input type="checkbox" name="is_active" value="true" class="minimal" readonly="readonly" {{$event->is_active == 1 ? 'checked' :''}}>  </td>
                      
                      
                      <td><a href="{{url('/admin/events')}}/{{ $event->id }}/edit" class="btn btn-primary"><i class="fa fa-edit"></i></a></td> 
                      <td>
                        <form action="{{url('/admin/events')}}/{{$event->id}}" method="POST" onsubmit="return confirm('Do you really want to Delete ?')">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                          <button type="submit" class="btn btn-danger">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                          </button>
                        </form>
                      </td>
                    </tr>
                     @empty
                    <tr>
                      <td colspan="7" class="text-center">
                        No data found  
                      </td>
                    </tr>
                  @endforelse
                </tbody>
                <tfoot>
                  <tr>

                  <th>Event Name</th>
                  <th>Event Description</th>                  
                  <th>Event Tumbnail</th>
                  <th>Event Date</th>
                  <th>Is Active</th>
                  <th>Edit</th>
                  <th>Delete</th>
                  
                  </tr>
                </tfoot>
              </table>
            
          </div>
          
            
            
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
    </div>
  </div>
@endsection
