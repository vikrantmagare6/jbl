@extends('admin.layouts.app')


@section('htmlheader_title')
  Events  -  {{$event->event_name}}
@endsection


@section('contentheader_title')
Events 
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li><a href="{{ url('/admin/events')}}">Events</a>  </li>
        <li class="active"> {{$event->event_name}} </li>
    </ol>
@endsection

@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              
              <form >
              {{ method_field('PUT') }}
              {!! csrf_field() !!}
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="event_name" readonly="readonly" value=" {!! $event->event_name !!}">
                </div> 
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="event_short_description" readonly="readonly" value="{!! $event->event_short_description !!}">
                </div> 
                
                <div class="col-md-12">
                  <img class="img-responsive" src="{{url('/public/uploads/images/')}}/{!! $event->event_large_img !!}">
                </div>
                <div class="col-md-12 margin-top-30 ">
                  {!! $event->event_content !!}
                </div>
                <div class="col-md-6">
                  
                  <input type="checkbox" name="is_active" class="minimal" value="true" {{$event->is_active == 1 ? 'checked' :''}}> Active
                </div> 
                <div class="col-md-12">
                
                  @forelse($event->imagesfile as $img)

                  <div class="col-xs-3">
                    <img src="{{ URL::asset('public/uploads/allimg/'.$img->images_url) }}" class="img-responsive" alt="">
                  </div>


                  @empty
                  <p>No data found</p>
                  @endforelse
                </div>
                <div class="col-md-12 margin-top-30">
                  
                  <a href="{{url('/admin/events')}}/{{ $event->id }}/edit" class="btn btn-primary btn-lg">Edit</a>
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection
