@extends('admin.layouts.app')


@section('htmlheader_title')
  Class
@endsection


@section('contentheader_title')
Class
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Class </li>
    </ol>
@endsection

@section('main-content')



  <div class="container-fluid spark-screen">
    <div class="row">
      <div class="">




        <!-- Default box -->
        <div class="box">
        
         
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0;">
          <div class="col-xs-12 margin-bottom-10 margin-top-20"> <a class="btn  btn-primary btn-lg" href="re-union/create">Add</a> </div>
          <div class="col-xs-12">

          <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Title</th>
                  <th>Tumbnail</th>                  
                  <th>Is Active</th>
                  <th>Edit</th>
                  <th>Delete</th>
       
                
                  
                </tr>
                </thead>
                <tbody>
                
                  @forelse($reunions as $reunion)
                    <tr>
                      <td>{!! $reunion->reunion_title !!}</td>                      
                      <td> <img width="200"  src="{{url('/public/uploads/images/')}}/{!! $reunion->reunion_thumbnail !!}" alt="reunion Tumbnail Image"> </td>   
                              
                      <td>   <input type="checkbox" name="is_active" value="true" class="minimal" readonly="readonly" {{$reunion->is_active == 1 ? 'checked' :''}}>  </td>
                      
                      
                      <td><a href="{{url('/admin/re-union')}}/{{ $reunion->id }}/edit" class="btn btn-primary"><i class="fa fa-edit"></i></a></td> 
                      <td>
                        <form action="{{url('/admin/re-union')}}/{{$reunion->id}}" method="POST" onsubmit="return confirm('Do you really want to Delete ?')">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                          <button type="submit" class="btn btn-danger">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                          </button>
                        </form>
                      </td>
                    </tr>
                     @empty
                    <tr>
                      <td colspan="5" class="text-center">
                        No data found  
                      </td>
                    </tr>
                  @endforelse
                </tbody>
                <tfoot>
                  <tr>

                  <th>Title</th>
                  <th>Tumbnail</th>                  
                  <th>Is Active</th>
                  <th>Edit</th>
                  <th>Delete</th>
       
                  </tr>
                </tfoot>
              </table>
            
          </div>
          
            
            
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
    </div>
  </div>
@endsection

