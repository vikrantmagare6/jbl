@extends('admin.layouts.app')


@section('htmlheader_title')
 Merchandise
@endsection


@section('contentheader_title')
Merchandise
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Merchandise </li>
    </ol>
@endsection

@section('main-content')



  <div class="container-fluid spark-screen">
    <div class="row">
      <div class="">




        <!-- Default box -->
        <div class="box">
        
         
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0;">
          <div class="col-xs-12 margin-bottom-10 margin-top-20"> <a class="btn  btn-primary btn-lg" href="{{ url('admin/merchandise/create') }}">Add</a> </div>
          <div class="col-xs-12">

          <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th> Name</th>                  
                  <th>Image 1</th>
                  <th>Image 2</th>
                  <th>Price</th>
                  <th>Is Active</th>
                  <th>Edit</th>
                  <th>Delete</th>

                </tr>
                </thead>
                <tbody>
                
                  @forelse($allMerchandise as $merchandise)
                    <tr>
                      <td>{!! $merchandise->m_name !!}</td>                      
                      <td> <img width="200"  src="{{url('/public/uploads/images/')}}/{!! $merchandise->m_image1 !!}" alt=" Image"> </td>                                               
                      <td> <img width="200"  src="{{url('/public/uploads/images/')}}/{!! $merchandise->m_image2 !!}" alt=" Image"> </td>                         
                      <td>   <input type="checkbox" name="is_active" value="true" class="minimal" readonly="readonly" {{$merchandise->is_active == 1 ? 'checked' :''}}>  </td>
                      <td>{!! $merchandise->m_price !!}</td>
                      <td><a href="{{url('/admin/merchandise')}}/{{ $merchandise->id }}/edit" class="btn btn-primary"><i class="fa fa-edit"></i></a></td> 
                      <td>
                        <form action="{{url('/admin/merchandise')}}/{{$merchandise->id}}" method="POST" onsubmit="return confirm('Do you really want to Delete ?')"> 
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                          <button type="submit" class="btn btn-danger">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                          </button>
                        </form>
                      </td>
                    </tr>
                     @empty
                    <tr>
                      <td colspan="6" class="text-center">
                        No data found  
                      </td>
                    </tr>
                  @endforelse
                </tbody>
                <tfoot>
                  <tr>

                  <th> Name</th>                  
                  <th>Image 1</th>
                  <th>Image 2</th>
                  <th>Price</th>
                  <th>Is Active</th>
                  <th>Edit</th>
                  <th>Delete</th>
                  
                  </tr>
                </tfoot>
              </table>
            
          </div>
          
            
            
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
    </div>
  </div>
@endsection
