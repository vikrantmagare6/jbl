@extends('admin.layouts.app')


@section('htmlheader_title')
Merchandise - Edit
@endsection


@section('contentheader_title')
Merchandise
@endsection

@section('contentheader_description')
Edit
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Merchandise </li>
    </ol>
@endsection

@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <?php //var_dump($banner) ?>
              <form enctype="multipart/form-data" method="POST" action="{{url('/admin/merchandise')}}/{{$merchand->id}}">
              {{ method_field('PUT') }}
              {!! csrf_field() !!}
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="m_name" placeholder="Name" value=" {!! $merchand->m_name !!}">
                </div> 

                <div class="col-md-12">
                 <img src="{{ url('/public/uploads/images/')}}/{!! $merchand->m_image1 !!}" class="img-responsive margin-bottom-30">
                
                
                  <label for="m_image1" style="display: inline;">Upload Image</label>
                  <input style="    display: inline-block;    margin-left: 20px;" type="file" id="m_image1" name="m_image1">
                  
                  
                </div> 
                <div class="col-md-12 margin-bottom-30">
                 <img src="{{ url('/public/uploads/images/')}}/{!! $merchand->m_image2 !!}" class="img-responsive margin-bottom-30">
                
                
                  <label for="m_image2" style="display: inline;">Upload Image</label>
                  <input style="    display: inline-block;    margin-left: 20px;" type="file" id="m_image2" name="m_image2">
                  
                  
                </div> 

                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="m_price" placeholder=" Price" value="{!! $merchand->m_price !!}">
                </div> 
                <div class="col-md-6 margin-top-30">                  
                  <input type="checkbox" name="is_active" class="minimal" value="true" {{$merchand->is_active == 1 ? 'checked' :''}}> Active
                </div> 
                  


                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Update">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection


@section('page_js')


<script type="text/javascript">
$(function () {
        CKEDITOR.replace('fa_content');
    });



  
</script>





@endsection