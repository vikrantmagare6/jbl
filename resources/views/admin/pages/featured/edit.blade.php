@extends('admin.layouts.app')


@section('htmlheader_title')
  Featured - Edit
@endsection


@section('contentheader_title')
Featured Alumni
@endsection

@section('contentheader_description')
Edit
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Featured Alumni </li>
    </ol>
@endsection

@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <?php //var_dump($banner) ?>
              <form enctype="multipart/form-data" method="POST" action="{{url('/admin/featured-alumni')}}/{{$featureda->id}}">
              {{ method_field('PUT') }}
              {!! csrf_field() !!}
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="fa_name" placeholder="Name" value=" {!! $featureda->fa_name !!}">
                </div> 
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="fa_designation" placeholder=" Short Description" value="{!! $featureda->fa_designation !!}">
                </div> 

                 


                <div class="col-md-12">
                 <img src="{{ url('/public/uploads/images/')}}/{!! $featureda->fa_image_url !!}" class="img-responsive margin-bottom-30">
                
                
                  <label for="exampleInputFile" style="display: inline;">Upload Image</label>
                  <input style="    display: inline-block;    margin-left: 20px;" type="file" id="fa_image_url" name="fa_image_url">
                  
                  
                </div> 

                <div class="col-md-12 margin-top-30">
                  <textarea id="fa_content" class="form-control" name="fa_content" rows="10" cols="80">{{$featureda->fa_content}}</textarea>
                </div>




                
                <div class="col-md-6 margin-top-30">                  
                  <input type="checkbox" name="is_active" class="minimal" value="true" {{$featureda->is_active == 1 ? 'checked' :''}}> Active
                </div> 
                  


                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Update">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection


@section('page_js')


<script type="text/javascript">
$(function () {
        CKEDITOR.replace('fa_content');
    });



  
</script>





@endsection