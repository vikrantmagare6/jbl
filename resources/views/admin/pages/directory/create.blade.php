@extends('admin.layouts.app')


@section('htmlheader_title')
  Alumni Directory - Create
@endsection

@section('page_css')

@endsection

@section('contentheader_title')
Alumni Directory 
@endsection

@section('contentheader_description')
Create
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Alumni Directory </li>
    </ol>
@endsection



@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <form enctype="multipart/form-data" method="POST" action="{{ url('admin/alumni-directory') }}">
              {!! csrf_field() !!}
              <div class="col-md-12">

              <div class="row">
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="a_name" placeholder="Name">
                </div> 

                <div class="col-md-6 ">                 
                  <input type="email" name="a_email" class="form-control margin-bottom-30 "  placeholder=" Email">
                </div> 
                
              </div>
                
              </div>

              <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="a_year" placeholder="Year">
                </div> 
                <div class="col-md-6">
                  <select name="a_cat"  class="form-control">
                  <option value="">Select Category</option>
                    <option value="teacher">Teacher</option>
                    <option value="normal">Normal</option>
                    <option value="silver">Silver</option>
                    <option value="honorary">Honorary</option>
                    <option value="founder">Founder</option>

                  </select>
                </div> 
              



                


                
                <div class="col-md-12">                  
                  <input type="checkbox" name="is_active" class="minimal" value="true">  Active
                </div> 
                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Submit">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection


@section('page_js')


<script type="text/javascript">
$(function () {
        CKEDITOR.replace('p_content');
    });
  

  $('.datepicker').datepicker({
      autoclose: true
    });
  
</script>





@endsection