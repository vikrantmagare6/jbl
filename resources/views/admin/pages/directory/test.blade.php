@extends('admin.layouts.app')


@section('htmlheader_title')
  Alumni Directory
@endsection


@section('contentheader_title')
test
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Alumni Directory </li>
    </ol>
@endsection

@section('main-content')



  <div class="container-fluid spark-screen">
    <div class="row">
      <div class="">




        <!-- Default box -->
        <div class="box">
        
         
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0;">
          <div class="col-xs-12 margin-bottom-10 margin-top-20"> <a class="btn  btn-primary btn-lg" href="{{ url('admin/alumni-directory/create') }}">Add</a> </div>
          <div class="col-xs-12">

         <table id="datatable">
           <thead>
             <tr>
               <th>ID</th>
               <th>First Name</th>
               <th>Email ID</th>
               <th>Year</th>
             </tr>
           </thead>
         </table>
            
          </div>
          
            
            
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
    </div>
  </div>
@endsection


@section('page_js')
<script type="text/javascript">
 
$(document).ready(function(){
  $('#datatable').DataTable();
});
</script>
@endsection