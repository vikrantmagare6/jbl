@extends('admin.layouts.app')


@section('htmlheader_title')
  Alumni Directory
@endsection


@section('contentheader_title')
Alumni Directory
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Alumni Directory </li>
    </ol>
@endsection

@section('main-content')



  <div class="container-fluid spark-screen">
    <div class="row">
      <div class="">




        <!-- Default box -->
        <div class="box">
        
         
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0;">
          <div class="col-xs-12 margin-bottom-10 margin-top-20"> <a class="btn  btn-primary btn-lg" href="{{ url('admin/alumni-directory/create') }}">Add</a> </div>
          <div class="col-xs-12">

          <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Namea</th>                  
                  <th>Email </th>
                  <th>Year</th>
                  <th>Category</th>
                  <th>Is Active</th>
                  <th>Edit</th>
                  <th>Delete</th>

                </tr>
                </thead>
                <tbody>
                
                  @forelse($allDirectory as $member)
                    <tr>
                      <td>{!! $member->a_name !!}</td>                      
                      <td> {!! $member->a_email !!}</td>
                      <td> {!! $member->a_year !!}</td>
                      <td> {!! $member->a_cat !!}</td>
                      <td>   <input type="checkbox" name="is_active" value="true" class="minimal" readonly="readonly" {{$member->is_active == 1 ? 'checked' :''}}>  </td>
                      
                      <td><a href="{{url('/admin/alumni-directory')}}/{{ $member->id }}/edit" class="btn btn-primary"><i class="fa fa-edit"></i></a></td> 
                      <td>
                        <form action="{{url('/admin/alumni-directory')}}/{{$member->id}}" method="POST" onsubmit="return confirm('Do you really want to Delete ?')">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                          <button type="submit" class="btn btn-danger">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                          </button>
                        </form>
                      </td>
                    </tr>
                     @empty
                    <tr>
                      <td colspan="7" class="text-center">
                        No data found  
                      </td>
                    </tr>
                  @endforelse
                </tbody>
                <tfoot>
                  <tr>

                  <th>Name</th>                  
                  <th>Email </th>
                  <th>Year</th>
                  <th>Category</th>
                  <th>Is Active</th>
                  <th>Edit</th>
                  <th>Delete</th>
                  
                  </tr>
                </tfoot>
              </table>
            
          </div>
          
            
            
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
    </div>
  </div>
@endsection
