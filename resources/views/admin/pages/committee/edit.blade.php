@extends('admin.layouts.app')


@section('htmlheader_title')
Committee - Edit
@endsection


@section('contentheader_title')
Committee
@endsection

@section('contentheader_description')
Edit
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Committee </li>
    </ol>
@endsection

@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <?php //var_dump($banner) ?>
              <form enctype="multipart/form-data" method="POST" action="{{url('/admin/committee')}}/{{$committee->id}}">
              {{ method_field('PUT') }}
              {!! csrf_field() !!}
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="committee_name" placeholder="Member Name" value=" {!! $committee->committee_name !!}">
                </div> 
                

                 <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="committee_designation" placeholder=" Button Text" value="{!! $committee->committee_designation !!}">
                </div> 
                <div class="col-md-12 margin-bottom-30">
                  
                  <textarea id="committee_description" class="form-control" name="committee_description" rows="10" cols="80">{!! $committee->committee_description !!}</textarea>
                </div> 
                
                <div class="col-md-12 margin-bottom-30">
                  
                  <img width="200"  src="{{url('/public/uploads/images/')}}/{!! $committee->committee_img !!}" alt=" Image">
                  

                </div> 



                <div class="col-md-12 margin-bottom-30">
                  <label for="committee_img" style="display: inline;">Upload Image</label>
                  <input style="    display: inline-block;    margin-left: 20px;" type="file" id="committee_img" name="committee_img">
                  
                </div>

                <div class="col-md-6 margin-bottom-30">
                  <select name="committee_category"  class="form-control">
                    <option value="founding" @if($committee->committee_category == 'founding'
) selected="selected" @endif >Founding</option>
                    <option value="advisery" @if($committee->committee_category == 'advisery'
) selected="selected" @endif >Advisery</option>
                    <option value="core" @if($committee->committee_category == 'core'
) selected="selected" @endif>Core</option>
                  </select>
                </div>


                <div class="col-md-6">
                  
                  <input type="checkbox" name="is_active" class="minimal" value="true" {{$committee->is_active == 1 ? 'checked' :''}}> Active
                </div> 
                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Update">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection

@section('page_js')


<script type="text/javascript">
$(function () {
        CKEDITOR.replace('committee_description');
    });
  
</script>
@endsection