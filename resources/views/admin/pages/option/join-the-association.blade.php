@extends('admin.layouts.app')


@section('htmlheader_title')
  Join The Association
@endsection


@section('contentheader_title')
Join The Association
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        
        <li class="active"> Join The Association </li>
    </ol>
@endsection

@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif

              
              <form enctype="multipart/form-data"  method="POST" action="{{ url('admin/store') }}">
              {!! csrf_field() !!}

              
              <div class="row margin-bottom-30">

                <div class="col-md-2">
                  <label class="labela">Content</label>
                </div> 
                <div class="col-md-10">
                    <textarea id="join_the_association" class="form-control" name="join_the_association" rows="10" cols="80">{{ $join_the_association->option_value }}</textarea>
                </div>
                                
              </div>

                               
                

                 
                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Submit">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection




@section('page_js')

<script type="text/javascript">
$(function () {
        CKEDITOR.replace('join_the_association');
    });
  
</script>

@endsection