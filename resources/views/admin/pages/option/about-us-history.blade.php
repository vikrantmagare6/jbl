@extends('admin.layouts.app')


@section('htmlheader_title')
  History
@endsection


@section('contentheader_title')
History
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li><a href="javascript:void(0);">About Us</a>  </li>
        <li class="active"> History </li>
    </ol>
@endsection

@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif

              
              <form enctype="multipart/form-data"  method="POST" action="{{ url('admin/store') }}">
              {!! csrf_field() !!}

              <div class="row margin-bottom-30">
                <div class="col-md-2 ">
                  <label class="labela">History Image</label>
                </div>
                <div class="col-md-10">
                  <img src=" {{url('/public/uploads/images/')}}/{!! $about_history_image->option_value !!}">
                </div>
              </div>

              <div class="row">
                <div class="col-md-2">
                  <label class="labela">Upload Image</label>
                </div> 
                <div class="col-md-10">                  
                  <input  type="file"  name="about_history_image"  >                                    
                  
                </div>                 
              </div>
              <div class="row">

                <div class="col-md-2">
                  <label class="labela">Description</label>
                </div> 
                <div class="col-md-10">
                    <textarea id="about_history_content" class="form-control" name="about_history_content" rows="10" cols="80">{{ $about_history_content->option_value }}</textarea>
                </div> 
                
              </div>

                               
                

                 
                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Submit">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection




@section('page_js')

<script type="text/javascript">
  $(function () {
    CKEDITOR.replace('about_history_content');
  });
  
</script>

@endsection