@extends('admin.layouts.app')


@section('htmlheader_title')
  Option
@endsection

@section('page_css')

@endsection

@section('contentheader_title')
Option
@endsection

@section('contentheader_description')
Banner Images Description
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Option
@endsection



@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <form method="POST" action="{{ url('admin/options') }}">
              {!! csrf_field() !!}
                <div class="col-md-6">
                  <input id="option_name" type="text" class="form-control margin-bottom-30" name="test" placeholder="Option name Name">
                </div> 
                <div class="col-md-6">
                  <input id="option_slug" readonly="readonly" type="text" class="form-control margin-bottom-30" name="option_name" placeholder="Option Slug">
                </div> 
                <div class="col-md-12 margin-top-30">
                  <textarea id="option_value" class="form-control" name="option_value" rows="10" cols="80"></textarea>
                </div>
                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Submit">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection


@section('page_js')

<script type="text/javascript">
  $(function () {
        CKEDITOR.replace('option_value');
    });

  $('#option_name').on('blur',function(){
    var temp1 = $(this).val();
    temp1 = temp1.replace(" ", "_");
    temp1 = temp1.toLowerCase();

    $('#option_slug').val(temp1);
    
  });


</script>



@endsection