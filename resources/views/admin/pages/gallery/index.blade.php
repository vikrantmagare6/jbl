@extends('admin.layouts.app')


@section('htmlheader_title')
  Gallery
@endsection


@section('contentheader_title')
Gallery 
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Gallery </li>
    </ol>
@endsection

@section('main-content')



  <div class="container-fluid spark-screen">
    <div class="row">
      <div class="">




        <!-- Default box -->
        <div class="box">
        
         
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0;">
          <div class="col-xs-12 margin-bottom-10 margin-top-20"> <a class="btn  btn-primary btn-lg" href="{{ url('admin/gallery/create') }}">Add</a> </div>
          <div class="col-xs-12">

          <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>Caption</th>
                  
                  <th>Delete</th>
       
                
                  
                </tr>
                </thead>
                <tbody>
                  @forelse($gallery as $gal)
                    <tr>
                      
                      
                      <td> <img width="200"  src="{{url('/public/uploads/images/')}}/{!! $gal->gallery_image !!}" alt="Gallery Image"> </td>               
                      <td>{!! $gal->gallery_caption !!}</td>   
                      
                      
                      
                       
                      <td>
                        <form action="{{url('/admin/gallery')}}/{{$gal->id}}" method="POST" onsubmit="return confirm('Do you really want to Delete ?')">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                          <button type="submit" class="btn btn-danger">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                          </button>
                        </form>
                      </td>
                    </tr>
                     @empty
                    <tr>
                      <td colspan="3" class="text-center">
                        No data found  
                      </td>
                    </tr>
                  @endforelse
                </tbody>
                <tfoot>
                  <tr>
                    <th>Image</th>
                    <th>Caption</th>                  
                    <th>Delete</th>
       
                  
                  </tr>
                </tfoot>
              </table>
            
          </div>
          
            
            
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
    </div>
  </div>
@endsection
