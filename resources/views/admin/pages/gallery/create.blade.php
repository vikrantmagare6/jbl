@extends('admin.layouts.app')


@section('htmlheader_title')
  Gallery - Create
@endsection


@section('contentheader_title')
Gallery
@endsection

@section('contentheader_description')
Create
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Gallery </li>
    </ol>
@endsection

@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <form enctype="multipart/form-data" method="POST" action="{{ url('admin/gallery') }}">
              {!! csrf_field() !!}
                <div class="col-md-12">
                  <input type="text" class="form-control margin-bottom-30" name="gallery_caption" placeholder="Gallery Caption">
                </div> 

                <div class="col-md-12 margin-bottom-30">
                  <label for="exampleInputFile" style="display: inline;">Upload Image</label>
                  <input style="    display: inline-block;    margin-left: 20px;" type="file" id="gallery_image" name="gallery_image">
                  
                </div>


                
                <div class="col-md-12 ">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Submit">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection
