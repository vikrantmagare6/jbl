@extends('admin.layouts.app')


@section('htmlheader_title')
  Form Submission
@endsection


@section('contentheader_title')
Form Submission 
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> {{$student->std_name}} </li>
    </ol>
@endsection

@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              
              <form >
              {{ method_field('PUT') }}
              {!! csrf_field() !!}
                <div class="col-md-6">
                  <label>Student Name</label>
                  <input type="text" class="form-control margin-bottom-30"  readonly="readonly" value=" {!! $student->std_name !!}">
                </div> 
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" readonly="readonly" value="{!! $student->std_email !!}">
                </div> 

                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" readonly="readonly" value="{!! $student->std_year_of_pass !!}">
                </div> 

                

                
                
                <div class="col-md-12">

                  <div class="row">

                  <div class="test" id="tk" data-token="{{csrf_token()}}"></div>  

                  

                  @forelse($student->imagesfile as $img)
                  
                  <div class="col-xs-3 margin-bottom-30">
                  <a href="{{ url('admin/download/image/') }}/{{$img->id }}"><img src="{{ URL::asset('public/uploads/allimg/'.$img->images_url) }}" class="img-responsive" alt=""></a>

                    
                  </div>


                  @empty
                  <p>No data found</p>
                  @endforelse
                    
                  </div>
                
                  


                </div>
                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection

@section('page_js')

<script type="text/javascript">


var token = $("#tk").attr('data-token')
var data = {_token: token};

function download(id){
  $.ajax({
      url: '{{ URL::to("/admin/download/image")}}/'+id,
      data: data,
      type: 'post',
      success: function (response) {
          
          console.log(response);
      }
  });

}

function download2(){
  $.ajax({
      url: '{{ URL::to("/admin/download/image")}}',
      data: data,
      type: 'get',
      success: function (response) {
          
          console.log(response);
      }
  });

}
  
</script>

@endsection
