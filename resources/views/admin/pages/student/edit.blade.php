@extends('admin.layouts.app')


@section('htmlheader_title')
  Form Submission - Edit
@endsection


@section('contentheader_title')
Form Submission
@endsection

@section('contentheader_description')
Edit
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Form Submission </li>
    </ol>
@endsection

@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <?php //var_dump($banner) ?>
              <form enctype="multipart/form-data" method="POST" action="{{url('/admin/re-union')}}/{{$reunion->id}}">
              {{ method_field('PUT') }}
              {!! csrf_field() !!}
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="reunion_title" placeholder="Re-union Name" value=" {!! $reunion->reunion_title !!}">
                </div> 
                
                 


                <div class="col-md-12">
                 <img src="{{ url('/public/uploads/images/')}}/{!! $reunion->reunion_thumbnail !!}" class="img-responsive margin-bottom-30">
                  <label for="reunion_thumbnail" style="display: inline;">Thumbnail Image</label>
                  <input style="    display: inline-block;    margin-left: 20px;" type="file" id="reunion_thumbnail" name="reunion_thumbnail">
                </div>               
                <div class="col-md-6 margin-top-30">                  
                  <input type="checkbox" name="is_active" class="minimal" value="true" {{$reunion->is_active == 1 ? 'checked' :''}}> Active
                </div> 
                <div class="col-md-12">

                <div class="row" id="tk" data-token="{{csrf_token()}}">
                     @if(isset($reunion) && count($reunion->imagesfile()) > 0)

                     @foreach( $reunion->imagesfile as $img )
                          <div class="col-md-3 image-thumbnail">
                          <button type="button" class="close" onclick="deleteImage({{$img->id}}, this)"><span>X</span></button>
                          <img class="img-thumbnail img-responsive" src="{{ URL::asset('public/uploads/allimg/'.$img->images_url) }}"/>
                          </div>
                     @endforeach

                     @else
                      <div class="col-md-6">
                          <label class="control-label">There are no product images</label>
                      </div>
                     @endif
                  </div>
                  
                </div> 
                  


                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Update">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection


@section('page_js')


<script type="text/javascript">


var token = $("#tk").attr('data-token')
var data = {_token: token};

function deleteImage(id, elem){
  $.ajax({
      url: '{{ URL::to("/admin/re-union/image")}}/'+id,
      data: data,
      type: 'post',
      success: function (response) {
          // alert('boom');
          $(elem).parent().remove();
          console.log(response);
      }
  });

}


  
</script>





@endsection