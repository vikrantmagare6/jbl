@extends('admin.layouts.app')


@section('htmlheader_title')
  Form Submission
@endsection


@section('contentheader_title')
Form Submission
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Form Submission </li>
    </ol>
@endsection

@section('main-content')



  <div class="container-fluid spark-screen">
    <div class="row">
      <div class="">




        <!-- Default box -->
        <div class="box">
        
         
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0;">
          
          <div class="col-xs-12 margin-top-30">

          <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email Address</th>                  
                  <th>Year Of Passing</th>                                    
                  <th>Delete</th>
       
                
                  
                </tr>
                </thead>
                <tbody>
                
                  @forelse($students as $student)
                    <tr>
                      
                      <td><a href="{{url('/admin/student-list')}}/{{ $student->id }}">{!! $student->std_name !!}</a></td>
                      <td>{!! $student->std_email !!}</td>                    
                      <td>{!! $student->std_year_of_pass !!}</td>                    
                      
                      <td>
                        <form action="{{url('/admin/student-list')}}/{{$student->id}}" method="POST" onsubmit="return confirm('Do you really want to Delete ?')">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                          <button type="submit" class="btn btn-danger">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                          </button>
                        </form>
                      </td>
                    </tr>
                     @empty
                    <tr>
                      <td colspan="4" class="text-center">
                        No data found  
                      </td>
                    </tr>
                  @endforelse
                </tbody>
                <tfoot>
                  <tr>

                  <th>Name</th>
                  <th>Email Address</th>                  
                  <th>Year Of Passing</th>                                    
                  <th>Delete</th>
       
                  </tr>
                </tfoot>
              </table>
            
          </div>
          
            
            
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
    </div>
  </div>
@endsection

