@extends('admin.layouts.app')


@section('htmlheader_title')
  Form Submission - create
@endsection

@section('page_css')

@endsection

@section('contentheader_title')
Form Submission
@endsection

@section('contentheader_description')
Create
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Form Submission </li>
    </ol>
@endsection



@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <form enctype="multipart/form-data" method="POST" action="{{ url('admin/re-union') }}">
              {!! csrf_field() !!}
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="reunion_title" placeholder="Re-union Title">
                </div> 
                <div class="col-md-6 margin-bottom-30">
                  <label for="reunion_thumbnail" style="display: inline;">Tumbnail Image</label>
                  <input style="    display: inline-block;    margin-left: 20px;" type="file" id="reunion_thumbnail" name="reunion_thumbnail">                  
                </div>
                 
                <div class="col-md-6">
                  
                  <input type="checkbox" name="is_active" class="minimal" value="true"> Active
                </div> 
                <div class="col-md-12">
                    <label class="control-label">Reunion Gallery</label>
                    <input id="reunion_gallery" name="reunion_gallery[]" type="file" multiple class="file-loading">
                </div>
                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Submit">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection


@section('page_js')


<script type="text/javascript">

  
      
      
  
  
</script>





@endsection