@extends('admin.layouts.app')


@section('htmlheader_title')
  Banner Image
@endsection


@section('contentheader_title')
Banner Images
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
    <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
    <li class="active"> Banner Images </li>
</ol>
@endsection

@section('main-content')



  <div class="container-fluid spark-screen">
    <div class="row">
      <div class="">




        <!-- Default box -->
        <div class="box">
        


   
         
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0;">
          <div class="col-xs-12 margin-bottom-10 margin-top-20"> <a class="btn  btn-primary btn-lg" href="{{ url('/admin/banner-images/create') }}">Add</a> </div>
          <div class="col-xs-12">

          <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Banner Heading</th>
                  <th>Banner Description</th>                  
                  <th>Banner Images</th>
                  <th>active</th>
                  <th>Edit</th>
                  <th>Delete</th>
       
                
                  
                </tr>
                </thead>
                <tbody>
                  @forelse($banners as $banner)
                    <tr>
                      <td>{!! $banner->banner_heading !!}</td>
                      <td>{!! $banner->banner_description !!}</td>   
                      <td> <img width="200"  src="{{url('/public/uploads/images/')}}/{!! $banner->banner_image !!}" alt="Banner Image"> </td>               
                      <td>   <input type="checkbox" name="is_active" value="true" class="minimal" readonly="readonly" {{$banner->is_active == 1 ? 'checked' :''}}>  </td>
                      
                      
                      <td><a href="{{url('/admin/banner-images')}}/{{ $banner->id }}/edit" class="btn btn-primary"><i class="fa fa-edit"></i></a></td> 
                      <td>
                        <form action="{{url('/admin/banner-images')}}/{{$banner->id}}" method="POST"  onsubmit="return confirm('Do you really want to Delete ?')">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                          <button type="submit" class="btn btn-danger" onClick="show_alert()">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                          </button>
                        </form>
                      </td>
                    </tr>
                     @empty
                    <tr>
                      <td colspan="6" class="text-center">
                        No data found  
                      </td>
                    </tr>
                  @endforelse
                </tbody>
                <tfoot>
                  <tr>

                  <th>Banner Heading</th>
                  <th>Banner Description</th>                  
                  <th>Banner Images</th>
                  <th>active</th>
                  <th>Edit</th>
                  <th>Delete</th>
                  
                  </tr>
                </tfoot>
              </table>
            
          </div>
          
            
            
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </div>
    </div>
  </div>
@endsection
