@extends('admin.layouts.app')


@section('htmlheader_title')
  Banner Image - Create
@endsection


@section('contentheader_title')
Banner Images
@endsection

@section('contentheader_description')
Create
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Banner Images </li>
    </ol>
@endsection

@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <form enctype="multipart/form-data" method="POST" action="{{ url('admin/banner-images') }}">
              {!! csrf_field() !!}
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="banner_heading" placeholder="Banner Heading">
                </div> 
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="banner_description" placeholder="Banner Description">
                </div> 

                 <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="banner_button_text" placeholder="Banner Button Text">
                </div> 
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="banner_button_link" placeholder="Banner Button Link">
                </div> 

                <div class="col-md-6">
                  <label for="exampleInputFile" style="display: inline;">Upload Image</label>
                  <input style="    display: inline-block;    margin-left: 20px;" type="file" id="banner_image" name="banner_image">
                  <p class="help-block"></p>
                </div>


                <div class="col-md-6">
                  
                  <input type="checkbox" name="is_active" class="minimal" value="true"> Active
                </div> 
                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Submit">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection
