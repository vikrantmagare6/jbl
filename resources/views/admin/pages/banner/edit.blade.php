@extends('admin.layouts.app')


@section('htmlheader_title')
  Banner Image - Edit
@endsection


@section('contentheader_title')
Banner Images
@endsection

@section('contentheader_description')
Edit
@endsection

@section('breadcrumb_li')
<ol class="breadcrumb">
        <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home </a></li>
        <li class="active"> Banner Images </li>
    </ol>
@endsection

@section('main-content')



    <div class="container-fluid spark-screen">
      <div class="row">    
        <div class="box">
          <div class="box-body" style="    padding-left: 0;    padding-right: 0;    padding-top: 0; padding-bottom: 0">
            <div class="form-container col-xs-12 padding-top-30 padding-bottom-30" >
              @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
              <?php //var_dump($banner) ?>
              <form enctype="multipart/form-data" method="POST" action="{{url('/admin/banner-images')}}/{{$banner->id}}">
              {{ method_field('PUT') }}
              {!! csrf_field() !!}
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="banner_heading" placeholder="Banner Heading" value=" {!! $banner->banner_heading !!}">
                </div> 
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="banner_description" placeholder="Banner Description" value="{!! $banner->banner_description !!}">
                </div> 

                 <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="banner_button_text" placeholder="Banner Button Text" value="{!! $banner->banner_button_text !!}">
                </div> 
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="banner_button_link" placeholder="Banner Button Link" value="{!! $banner->banner_button_link !!}">
                </div> 
                <div class="col-md-6 margin-bottom-30">
                  
                  <img width="200" src="{{url('/public/uploads/images/')}}/{!! $banner->banner_image !!}" alt="Banner Image">
                  <input type="hidden" name="banner_e_image" value="{!! $banner->banner_image !!}">

                </div> 


                <div class="col-md-6">
                  <label for="exampleInputFile" style="display: inline;">Upload Image</label>
                  <input style="    display: inline-block;    margin-left: 20px;" type="file" id="banner_image" name="banner_image">
                  <p class="help-block"></p>
                </div>


                <div class="col-md-6">
                  
                  <input type="checkbox" name="is_active" class="minimal" value="true" {{$banner->is_active == 1 ? 'checked' :''}}> Active
                </div> 
                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Update">
                </div>                
              </form>
            </div><!-- form container -->
          </div> <!-- box-body -->       
        </div><!-- /.box -->
      </div> <!-- row -->
    </div> <!-- container-fluid -->
  
@endsection
