<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<!-- <script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script> -->

<script type="text/javascript" src="{{ url('public/js/bootstrap-datepicker.min.js') }}"></script>
<!-- <script type="text/javascript" src="{{ url('public/js/semantic.min.js') }}"></script> -->
<!-- bootstrap time picker -->
<script src="{{ url('public/js/bootstrap-timepicker.min.js') }}"></script>

<script type="text/javascript" src="{{ url('public/js/fileinput.js') }}"></script>
<script type="text/javascript" src="{{ url('public/js/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ url('public/js/custom.js') }}"></script>


<script type="text/javascript" scr="{{ url('public/js/jquery.dataTables.min.js') }}"></script>



<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->

      @yield('page_js') 
