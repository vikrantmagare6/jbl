<head>
    <meta charset="UTF-8">
    <title> JBPAA - @yield('htmlheader_title', 'Your title here') </title>

    <link rel="shortcut icon" href="{{ url('public/img/favicon.ico')}}" type="image/x-icon">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

    <!-- Bootstrap time Picker -->

    <link rel="stylesheet" href="{{ url('public/css/bootstrap-timepicker.min.css') }}">


    
    
    <link rel="stylesheet" type="text/css" href="{{ url('public/css/fileinput.css') }}"> 
    <link rel="stylesheet" type="text/css" href="{{ url('public/css/bootstrap-datepicker.min.css') }}"> 
    <!-- <link rel="stylesheet" type="text/css" href="{{ url('public/css/semantic.min.css') }}">  -->

    <link href="{{ asset('public/css/all.css') }}" rel="stylesheet" type="text/css" />

    
    <link rel="stylesheet" type="text/css" href="{{ url('public/css/custom.css') }}">
    

    @yield('page_css')
    


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via ile:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        //See https://laracasts.com/discuss/channels/vue/use-trans-in-vuejs
        window.trans = @php
            // copy all translations from /resources/lang/CURRENT_LOCALE/* to global JS variable
            $lang_files = File::files(resource_path() . '/lang/' . App::getLocale());
            $trans = [];
            foreach ($lang_files as $f) {
                $filename = pathinfo($f)['filename'];
                $trans[$filename] = trans($filename);
            }
            $trans['adminlte_lang_message'] = trans('adminlte_lang::message');
            echo json_encode($trans);
        @endphp
    </script>
</head>
