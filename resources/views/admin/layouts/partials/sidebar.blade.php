<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
<!--  -->        <!-- search form (Optional) -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Menu</li>
            <!-- Optionally, you can add icons to the links -->
            <!-- <li  class="{{ Request::is('admin') ? 'active' : (Request::is('admin/*'))  ? 'active' : '' }}"><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li> -->
            <li class="{{ Request::is('admin/banner-images') ? 'active' : (Request::is('admin/banner-images/*'))  ? 'active' : '' }}"><a href="{{ url('admin/banner-images') }}"><i class='fa fa-link'></i> <span>Banner Images</span></a></li>
            <li class="{{ Request::is('admin/home') ? 'active' : (Request::is('admin/home/*'))  ? 'active' : '' }}"><a href="{{ url('admin/home') }}"><i class='fa fa-link'></i> <span>Home Text</span></a></li>
            
            <li class="{{ Request::is('admin/events') ? 'active' : (Request::is('admin/events/*'))  ? 'active' : '' }}"><a href="{{ url('admin/events') }}"><i class='fa fa-link'></i> <span>Events</span></a></li>
            
            <li class="treeview">
                <a href="url('alumni-directory')"><i class='fa fa-link'></i> <span>About Us</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ Request::is('admin/history') ? 'active' : (Request::is('admin/history/*'))  ? 'active' : '' }}"><a href="{{url('admin/history')}}"><i class="fa fa-circle-o"></i><span>History</span></a></li>
                    <li class="{{ Request::is('admin/principal') ? 'active' : (Request::is('events/*'))  ? 'active' : '' }}"><a href="{{url('admin/principal')}}"><i class="fa fa-circle-o"></i><span>Principal</span></a></li>
                    <li class="{{ Request::is('admin/committee') ? 'active' : (Request::is('admin/committee/*'))  ? 'active' : '' }}"><a href="{{ url('admin/committee') }}"><i class="fa fa-circle-o"></i><span>Committee</span></a></li>
                </ul>
            </li>
            
            <li class="{{ Request::is('admin/gallery') ? 'active' : (Request::is('admin/gallery/*'))  ? 'active' : '' }}"><a href="{{ url('admin/gallery') }}"><i class='fa fa-link'></i> <span>Gallery</span></a></li>
            
            <li class="treeview">
                <a href="javascript:void(0);"><i class='fa fa-link'></i> <span>Re-Union</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ Request::is('admin/re-union') ? 'active' : (Request::is('admin/re-union/*'))  ? 'active' : '' }}"><a href="{{url('admin/re-union')}}"><i class="fa fa-circle-o"></i><span>Class</span></a></li> 
                    <li class="{{ Request::is('admin/student-list') ? 'active' : (Request::is('admin/student-list/*'))  ? 'active' : '' }}"><a href="{{url('admin/student-list')}}"><i class="fa fa-circle-o"></i><span>Form Submission</span></a></li>                    
                </ul>
            </li>
             <li class="treeview">
                <a href="javascript:void(0);"><i class='fa fa-link'></i> <span>Connect</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ Request::is('admin/featured-alumni') ? 'active' : (Request::is('admin/featured-alumni/*'))  ? 'active' : '' }}"><a href="{{url('admin/featured-alumni')}}"><i class="fa fa-circle-o"></i><span>Featured Alumni</span></a></li>                    
                    <li class="{{ Request::is('admin/merchandise') ? 'active' : (Request::is('admin/merchandise/*'))  ? 'active' : '' }}"><a href="{{url('admin/merchandise')}}"><i class="fa fa-circle-o"></i><span>Merchandise</span></a></li>                    
                    <li class="{{ Request::is('admin/project') ? 'active' : (Request::is('admin/project/*'))  ? 'active' : '' }}"><a href="{{url('admin/project')}}"><i class="fa fa-circle-o"></i><span>Project</span></a></li>                    
                    <li class="{{ Request::is('admin/alumni-directory') ? 'active' : (Request::is('admin/alumni-directory/*'))  ? 'active' : '' }}"><a href="{{ url('admin/alumni-directory') }}"><i class="fa fa-circle-o"></i><span>Alumni Directory</span></a></li>                                        
                </ul>
            </li>
            <li class="{{ Request::is('admin/contact') ? 'active' : (Request::is('admin/contact/*'))  ? 'active' : '' }}"><a href="{{ url('admin/contact') }}"><i class='fa fa-link'></i> <span>Contact List</span></a></li>
            <li class="treeview">
                <a href="javascript:void(0);"><i class='fa fa-link'></i> <span>Others</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ Request::is('admin/what-we-need') ? 'active' : (Request::is('admin/what-we-need/*'))  ? 'active' : '' }}"><a href="{{url('admin/what-we-need')}}"><i class="fa fa-circle-o"></i><span>What We Need</span></a></li>
                    <li class="{{ Request::is('admin/join-the-association') ? 'active' : (Request::is('join-the-association/*'))  ? 'active' : '' }}"><a href="{{url('admin/join-the-association')}}"><i class="fa fa-circle-o"></i><span>Join The Association</span></a></li>
                    
                </ul>
            </li>
            <!-- <li class="{{ Request::is('gallery') ? 'active' : (Request::is('gallery/*'))  ? 'active' : '' }}"><a href="{{ url('admin/options/create') }}"><i class='fa fa-link'></i> <span>Options</span></a></li>
             -->

            
            
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
