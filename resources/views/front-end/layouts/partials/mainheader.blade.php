<div class="full-header-wrapper">
  <div class="outer-wrapper">
    <div class="inner-wrapper">
      <a class="text-uppercase" href="{{url('join-the-association')}}"><i class="fa fa-users" aria-hidden="true"></i><span>join the association</span></a>
      <span>|</span>  
      <a class="text-uppercase" href="{{url('contact-us')}}"><i class="fa fa-phone" aria-hidden="true"></i><span>contact us</span></a>
    </div>
  </div>

  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="{{ url('/') }}/"><img src="{{url('public/img/logo.png')}}"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
          <li  class="{{ Request::is('about-us') ? 'active' : (Request::is('about-us/*'))  ? 'active' : '' }}" ><a href="{{url('about-us')}}">about us</a></li>

          <li  class="{{ Request::is('events') ? 'active' : (Request::is('events/*'))  ? 'active' : '' }}" ><a href="{{url('events')}}">events</a></li>
          <li  class="{{ Request::is('gallery') ? 'active' : (Request::is('gallery/*'))  ? 'active' : '' }}" ><a href="{{url('gallery')}}">gallery</a></li>
          <li  class="{{ Request::is('reunion') ? 'active' : (Request::is('reunion/*'))  ? 'active' : '' }}" ><a href="{{url('reunion')}}">reunion</a></li>
          <li  class="{{ Request::is('connect') ? 'active' : (Request::is('connect/*'))  ? 'active' : '' }}" ><a href="{{url('connect')}}">connect</a></li>
          <li  class="{{ Request::is('what-we-need') ? 'active' : (Request::is('what-we-need/*'))  ? 'active' : '' }}" ><a href="{{url('what-we-need')}}">what we need</a></li>
        </ul>
      </div>
    </div>
  </nav>
</div>