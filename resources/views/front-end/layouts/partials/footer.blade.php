<div class="site-footer">
  	<footer>
		<section class="foo">
		    <div class="container-fluid">
		        <div class="row">
		            <div class="col-sm-9 foo1">
		                <span>Sign Up For Newsletters</span>
		                <form class="subscribe-form" action="//venjaracarpets.us15.list-manage.com/subscribe/post?u=0d07c866d0e9d7e202fb32991&amp;id=4be5e62eaa" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">        
		                <!-- <span>Sign Up For Newsletters</span>  -->           
		                    <input class="form-control" type="email" pattern="[^ @]*@[^ @]*" name="EMAIL" id="contes3" placeholder="Email-id" required="">
		                    <button id="email-submit" type="submit" title="Subscribe" class="btn-subscribe!"><i style="vertical-align: middle;" class="fa fa-chevron-right" aria-hidden="true"></i></button>
		                </form>
		            </div>
		            <div class="col-sm-3 tab-height">
		                <div class="social-icons">
		                    <span><i class="fa fa-facebook" aria-hidden="true"></i></span>
		                    <span><i class="fa fa-twitter" aria-hidden="true"></i></span>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
	    <!-- <div class="social-icons">
	        <span><i class="fa fa-facebook" aria-hidden="true"></i></span>
	        <span><i class="fa fa-twitter" aria-hidden="true"></i></span>
	    </div> -->
	    <ul class="footer-menu">
	        <li><a href="{{url('/')}}">home</a></li>
	        <li><a href="{{url('about-us')}}">about us</a></li>
	        <li><a href="{{url('events')}}">events</a></li>
	        <li><a href="{{url('gallery')}}">gallery</a></li>
	        <li><a href="{{url('reunion')}}">reunion</a></li>
	        <li><a href="{{url('connect')}}">connect</a></li>
	        <li><a href="{{url('what-we-need')}}">what we need</a></li>
	        <li><a href="{{url('join-the-association')}}">join the association</a></li>
	        <li><a href="{{url('contact-us')}}">contact us</a></li>
	    </ul>
	    <p class="copyright">
	        <span>&copy;Copyright 2017 J.B Petit Alumni Association.</span>
	        <span>&nbsp;All rights reserved.</span>
	        <span>Crafted by <a href="https://www.togglehead.in/" target="_blank">Togglehead</a></span>
	    </p>
	</footer>
</div>