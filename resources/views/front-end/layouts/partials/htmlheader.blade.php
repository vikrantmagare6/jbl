<head>
  <meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="description" content="">

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<title> JBPAA - @yield('htmlheader_title', 'J.B. Petit Alumni Association') </title>

<link rel="shortcut icon" type="image/x-icon" href="{{url('public/dist/img/favicon.ico')}}" />

<link href="https://fonts.googleapis.com/css?family=Open+Sans|Questrial" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


<link rel="stylesheet" type="text/css" href="{{ url('public/dist/css/app.min.css') }}"> 
<link rel="stylesheet" type="text/css" href="{{ url('public/css/fcustom.css') }}"> 

@yield('page_css')

<!--[if lt IE 9]>
    <script src="js/vendor/html5-3.6-respond-1.4.2.min.js"></script>
<![endif]--></head>
