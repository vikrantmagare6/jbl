

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->


@section('htmlheader')
    @include('front-end.layouts.partials.htmlheader')
@show


<body >

<!--[if lt IE 8]>
    <p class="browserupgrade">
        You are using an <strong>outdated</strong> browser.
        Please <a href="http://browsehappy.com/"> upgrade your browser </a>
        to improve your experience.
    </p>
<![endif]--> 


    @include('front-end.layouts.partials.mainheader')
    
        <!--  insert body contenta  -->

            @yield('main-content')
        <!--  end body content -->
        

    @include('front-end.layouts.partials.footer')


@section('scripts')
    @include('front-end.layouts.partials.scripts')
@show

</body>
</html>
