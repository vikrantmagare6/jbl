@extends('front-end.layouts.app')
@section('htmlheader_title')
    Contact
@endsection

@section('page_css')
 <script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection

@section('main-content') 

<div class="heading-container">
                <h3 class="common-heading">contact us</h3>
            </div>
             @if(count($errors) > 0)
                <div class="row">
                  <div class="col-md-6">
                    <ul>
                    @foreach($errors->all() as $error)
                     <li>{{$error}}</li>
                    @endforeach
                      
                    </ul>
                  </div>      
                </div>
              @endif
            <form class="contact-form" id="main-form43" method="POST" action="{{ url('contact-us') }}">
            {!! csrf_field() !!}
                <div class="field-container">
                    <label for="c_name">*NAME</label>
                    <input id="name" type="text" name="c_name">
                </div>
                <div class="field-container">
                    <label for="c_email_id">*EMAIL ID</label>
                    <input id="mail" type="email" name="c_email_id">
                </div>
                <div class="field-container">
                    <label for="c_number">*NUMBER</label>
                    <input id="number" type="text" name="c_number" maxlength="15">
                </div>
                <div class="field-container">
                    <label for="message">*MESSAGE</label>
                    <textarea id="message" name="c_message" rows="4"></textarea>
                </div>
                <div class="text-center">
                    <div class="captcha-block">
                        <div class="g-recaptcha" data-sitekey="6LfuFTIUAAAAAOlDoSj1ZWvYH74XPAPY3bqUkfl6"></div>
                        <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                    </div>
                </div>
                <div class="text-center">
                    <!-- <input type="submit" name="submit" /> -->
                    <button class="button-outer" type="submit">SUBMIT</button>
                </div>
            </form>

    





@endsection


@section('page_js')

<script type="text/javascript">
    $("#main-form43").validate({
        // ignore: ".ignore",
                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side
                    c_name: {
                        required: true,
                        
                    },
                    c_email_id: {
                        required: true,
                        email: true
                    },
                    c_number: {
                        required: true,
                        digits: true,
                        
                        maxlength: 15
                    },
                    c_message:{
                        required: true,
                        
                    },
                    // hiddenRecaptcha: {
                    //      required: function() {
                    //          if (grecaptcha.getResponse() == '') {
                    //              return true;
                    //          } else {
                    //              return false;
                    //          }
                    //      }
                    //  }
                    

                },

                // Specify validation error messages
                messages: {
                    c_name: {
                        required: "Please Enter Your Firstname",
                        
                    },
                    c_email_id: {
                        
                        maxlength: "Enter 15 digit number"
                    },
                    // std_email: "Please Enter a Valid Email Address",
                    // hiddenRecaptcha : {
                    //     required: "Please select Captch",
                    // },                    
                    c_number : {
                        required: "Please Enter Your Firstname",
                    },
                    c_message : {
                        required: "Please Enter Your Firstname",
                    }
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function(form) {
                    form.submit();
                }
            });
</script>
@endsection