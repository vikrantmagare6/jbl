@extends('front-end.layouts.app')
@section('htmlheader_title')
Home
@endsection

@section('main-content') 



<section class="slider-home">
  <div id="myCarousel-1" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">

      @foreach($slider as $slide)
      
      <li data-target="#myCarousel-1" data-slide-to="{{ $loop->index }}" class=" @if($loop->first) active @endif"></li>
      @endforeach

      
      
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">



      

      
      @forelse($slider as $slide)

      <div class="item @if ($loop->first) active @endif" style="background-image: url('public/uploads/images/{{$slide->banner_image}}')">
        <div class="carousel-caption">
          <div class="outer-valign">
            <div class="inner-valign">
              <img src="http://via.placeholder.com/30x30">
              <h3>{{ $slide->banner_heading}}</h3>
              <p>{{ $slide->banner_description}}</p>
              <a class="button-outer" href="{{ $slide->banner_button_link}}">{{ $slide->banner_button_text}}</a>
            </div>
          </div>
        </div>
      </div>

      @empty
      <tr>
        <td colspan="6" class="text-center">
          No data found  
        </td>
      </tr>


      @endforelse

      
      
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel-1" data-slide="prev">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel-1" data-slide="next">
      <i class="fa fa-chevron-right" aria-hidden="true"></i>
      <span class="sr-only">Next</span>
    </a>
  </div>
</section>



<section class="home-alumni">    
  <div class="container">
    <div class="row">
      <div class="col-sm-2">
        <img src="{{url('public/img/lamp.png')}}">
      </div>
      <div class="col-sm-10">
        <div class="content">
          <h2>{{ $home_page_title->option_value }}</h2>
          {!! $home_page_description->option_value !!}
        </div>
      </div>
    </div>
  </div>
</section>

<section class="container">
  



  



  
  <?php if (isset($upcomingEvent->event_name)): ?>

    <div class="heading-container">
     <h3 class="common-heading">recent event</h3>
   </div>

   <div class="row">
    <div class="col-sm-4 col-md-3">
      <div>
        <img src="{{url('public/uploads/images')}}/{{ $upcomingEvent->event_large_img }}" class="img-responsive home-event-img" alt="Naheed Divecha">
        
      </div>
    </div>


    
    
    


    <div class="col-sm-8 col-md-9">
      <article class="home-event">
        <h3>{{$upcomingEvent->event_name}}</h3>
        <?php 
        $dt = Carbon\Carbon::parse($upcomingEvent->event_date_time);
        ?>
        <?php if ($dt->minute  == 0 ) {
          $minutea = '00';
        }else {
          $minutea = $dt->minute;
        }
        if ($dt->second == 0) {
          $seconda =  '00';
        }else{
          $seconda =  $dt->second;

        }
        
        $timea = $dt->hour.':'.$minutea.':'.$seconda;
        $date = $dt->day."-".$dt->month."-".$dt->year." ".$timea;
        

        
        ?>
        <?php 
                // $myDateTime = DateTime::createFromFormat('y-m-d H:i:s', $upcomingEvent->event_date_time);
                // $formattedweddingdate = $myDateTime->format('dS F Y');
                // $time = $myDateTime->format('h:i:s');

                // $myDateTime = DateTime::createFromFormat('y-m-d h:i', $upcomingEvent->event_date_time);
                //   $formattedweddingdate = $myDateTime->format('dS F Y');
        ?>
        <p class="date-time"><i class="fa fa-calendar" aria-hidden="true"></i><span><?php echo Datetime::createFromFormat('d-m-Y H:i:s', $date)->format('dS F Y'); ?>  </span></p>
        <p class="date-time"><i class="fa fa-clock-o" aria-hidden="true"></i><span> <?php echo Datetime::createFromFormat('d-m-Y H:i:s', $date)->format('h:i A'); ?></span></p>
        <div class="e-descript">
          {!! $upcomingEvent->event_short_description !!}   
        </div>
        
        <a class="button-outer" href="{{ url('/events') }}/{{ $upcomingEvent->id }}">view page</a>
      </article>
    </div>
  </div> <!-- row -->


<?php endif ?>









</section>

@endsection