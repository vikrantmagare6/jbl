



        @extends('front-end.layouts.app')
@section('htmlheader_title')
    Events
@endsection

@section('main-content') 

        <div class="tab-wrapper">
            <div class="container">
                <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs nav-tabs-responsive" role="tablist" style="margin:40px 0 0 0;">
                        <li role="presentation" class="active">
                            <a href="#past-events" id="past-events-tab" role="tab" data-toggle="tab" aria-controls="past-events" aria-expanded="true">
                                <span class="text">PAST EVENTS</span>
                            </a>
                        </li>
                        <li role="presentation" class="next">
                            <a href="#upcoming-events" role="tab" id="upcoming-events-tab" data-toggle="tab" aria-controls="upcoming-events">
                                <span class="text">UPCOMING EVENTS</span>
                            </a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" id="past-events" class="tab-pane fade active in" aria-labelledby="past-events-tab">


                        

                        @foreach($past_event as $past_event)

                        
                        <?php 
                        $dt = Carbon\Carbon::parse($past_event->event_date_time);

                        if ($dt->minute  == 0 ) {
                            $minutea = '00';
                          }else {
                            $minutea = $dt->minute;
                          }
                          if ($dt->second == 0) {
                            $seconda =  '00';
                          }else{
                            $seconda =  $dt->second;

                          }

                        $timea = $dt->hour.':'.$minutea.':'.$seconda;
                        $date = $dt->day."-".$dt->month."-".$dt->year." ".$timea;
                         ?>
                        

                        <div class="row">
                            <div class="col-xs-12 col-sm-3 gallery-anchor project-anchor">
                                    <img src="{{url('public/uploads/images')}}/{{$past_event->event_large_img}}" class="img-responsive">
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="project-div">
                                    <h3 class="name">{{ $past_event->event_name }}</h3>
                                    <h4 class="batch"><?php echo Datetime::createFromFormat('d-m-Y H:i:s', $date)->format('dS F Y'); ?></h4>
                                    <div class="line"></div>
                                    <p>{{ $past_event->event_short_description }}</p>
                                    
                                    <a href="{{ url('/events') }}/{{ $past_event->id }}" class="button-outer">view event</a>
                                </div>
                            </div>
                        </div>



                        @endforeach


                            
                           
                        </div>
                        <div role="tabpanel" id="upcoming-events" class="tab-pane fade" aria-labelledby="upcoming-events-tab">


                        <?php //var_dump() ?>
                        <?php if (count($upcoming_event)): ?>
                            

                            
                              @foreach($upcoming_event as $upcoming_event)

                              <?php 
                                $dt = Carbon\Carbon::parse($upcoming_event->event_date_time);

                                if ($dt->minute  == 0 ) {
                                    $minutea = '00';
                                  }else {
                                    $minutea = $dt->minute;
                                  }
                                  if ($dt->second == 0) {
                                    $seconda =  '00';
                                  }else{
                                    $seconda =  $dt->second;

                                  }

                                $timea = $dt->hour.':'.$minutea.':'.$seconda;
                                $date = $dt->day."-".$dt->month."-".$dt->year." ".$timea;
                                 ?>

                        <div class="row">
                            <div class="col-xs-12 col-sm-3 gallery-anchor project-anchor">
                                    <img src="{{url('public/uploads/images')}}/{{$upcoming_event->event_large_img}}" class="img-responsive">
                            </div>
                            <div class="col-xs-12 col-sm-9">
                                <div class="project-div">
                                    <h3 class="name">{{ $upcoming_event->event_name }}</h3>
                                    <h4 class="batch"><?php echo Datetime::createFromFormat('d-m-Y H:i:s', $date)->format('dS F Y'); ?></h4>
                                    <div class="line"></div>
                                    <p>{{ $upcoming_event->event_short_description }}</p>
                                    
                                    <a href="{{ url('/events') }}/{{ $past_event->id }}" class="button-outer">view event</a>
                                </div>
                            </div>
                        </div>



                        @endforeach

                        <?php else: ?>

                            <h2 style="text-align: center;">NO Upcoming Event</h2>

                        <?php endif ?>


                        
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection