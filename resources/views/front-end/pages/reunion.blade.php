@extends('front-end.layouts.app')
@section('htmlheader_title')
    Reunion
@endsection

@section('page_css')
 <script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection

@section('main-content') 



<div class="container">
                <div class="heading-container">
                    <h3 class="common-heading">reunion</h3>
                </div>
                <div class="tab-wrapper">
                    <div class="container" style="padding: 0;">
                        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab" class="nav nav-tabs nav-tabs-responsive" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#history" id="history-tab" role="tab" data-toggle="tab" aria-controls="history" aria-expanded="true">
                                        <span class="text">CLASS REUNION</span>
                                    </a>
                                </li>
                                <li role="presentation" class="next">
                                    <a href="#principal" role="tab" id="principal-tab" data-toggle="tab" aria-controls="principal">
                                        <span class="text">ADDING YOUR PICTURES</span>
                                    </a>
                                </li>
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="history" aria-labelledby="history-tab">
                                    <div class="row">

                                    @foreach($reunion as $reunion)


                                    @if($reunion->reunion_thumbnail != "default.png")


                                    <div class="reunion-gall col-xs-12 col-sm-3 gallery-anchor">
                                            <a href="{{ url('/public/uploads/images')}}/{{$reunion->reunion_thumbnail}}">
                                                <img src="{{ url('/public/uploads/images')}}/{{$reunion->reunion_thumbnail}}" class="img-responsive" />
                                                <div class="overlay">
                                                    <p><span>view album</span><span>{{$reunion->reunion_title}}</span></p>
                                                </div>
                                            </a>

                                            @if(isset($reunion) && count($reunion->imagesfile()) > 0)
                                            @foreach( $reunion->imagesfile as $img )

                                                <a href="{{ url('public/uploads/allimg')}}/{{ $img->images_url}}">
                                                    <img src="{{ url('public/uploads/allimg')}}/{{ $img->images_url}}" class="img-responsive" style="display: none;" />
                                                </a>


                                            @endforeach
                                            @endif
                                            
                                            
                                        </div>




                                    @else

                                    <div class="col-xs-12 col-sm-3 gallery-anchor opps1">
                                            <a href="javascript:void(0);">
                                                <img src="http://via.placeholder.com/360x360" class="img-responsive" />
                                                <div class="oops">
                                                    <p><span>{{$reunion->reunion_title}}</span></p>
                                                </div>
                                                <div class="overlay">
                                                    <p><span>Oops no pictures found...!!</span><span>Send us your pictures!</span></p>
                                                </div>
                                            </a>
                                        </div>
                                    @endif
                                    

                                        


                                    @endforeach
                                    
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="principal" aria-labelledby="principal-tab">
                                    <div class="adding-pic">
                                        <p>The Class Reunion page is an interactive page where members of the Alumni organization can upload photographs from school as well as current reunions.</p>

                                        <form class="contact-form" id="adding-picture22" enctype="multipart/form-data" method="POST" action="{{ url('re-union') }}">
                                        {!! csrf_field() !!}
                                            <div class="field-container">
                                                <label for="name">*NAME</label>
                                                <input id="name" type="text" name="std_name">
                                            </div>
                                            <div class="field-container">
                                                <label for="mail">*EMAIL ID</label>
                                                <input id="mail" type="email" name="std_email">
                                            </div>
                                            <div class="field-container">
                                                <label for="number">*batch year</label>
                                                <input id="number" type="text" name="std_year_of_pass">
                                            </div>
                                            <div class="field-container">
                                                <label for="file">photograph(s)</label>
                                                <input id="file" type="file" name="std_gallery[]" multiple>
                                            </div>
                                            <div class="text-center">
                                            <div class="captcha-block">
                                                <div class="g-recaptcha" data-sitekey="6LeYDikUAAAAALsTUekbWwFQ0bNbUq91km72-r5T"></div>
                                                <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                                            </div>
                                        </div>
                                            <div class="text-center">
                                                <button class="button-outer" type="submit">SUBMIT</button>
                                            </div>
                                        </form>














































                                        <p>Only members of The J.B. Petit Alumni Association can use this facility.</p>
                                        <p>If you are not yet a member, use the website to download a form, fill it in, and send it to us.</p>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    

@endsection

@section('page_js')

<script type="text/javascript">
    $("#adding-picture22").validate({
        ignore: ".ignore",
                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side
                    std_name: {
                        required: true,
                        
                    },
                    std_email: {
                        required: true,
                        email: true
                    },
                    std_year_of_pass: {
                        required: true,
                        digits: true,
                        minlength: 4,
                        maxlength: 4
                    },
                    std_gallery:{
                        required: true,
                        extension: "jpeg|png|jpg"
                    },
                    hiddenRecaptcha: {
                         required: function() {
                             if (grecaptcha.getResponse() == '') {
                                 return true;
                             } else {
                                 return false;
                             }
                         }
                     }
                    

                },

                // Specify validation error messages
                messages: {
                    std_name: {
                        required: "Please Enter Your Firstname",
                        
                    },
                    std_year_of_pass: {
                        minlength: "Enter 4 digit number",
                        maxlength: "Enter 4 digit number"
                    },
                    // std_email: "Please Enter a Valid Email Address",
                    hiddenRecaptcha : {
                        required: "Please select Captch",
                    },
                    std_gallery : {

                        required: "Please Enter Your Firstname",
                        extension : "Select JPG PNG"

                    }
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function(form) {
                    form.submit();
                }
            });
</script>
@endsection