



@extends('front-end.layouts.app')
@section('htmlheader_title')
    404
@endsection

@section('main-content') 

<style type="text/css">
.thank-you-c {

    height: calc(100% - 390px);
    margin-bottom: 70px;
    margin-top: 70px;
}

@media  (max-width: 500px) {
    .thank-you-c {
        margin-bottom: 110px;
        margin-top: 140px;
        height: auto !important;
    }
    
}
    
</style>



	<div class="container thank-you-c" style="    ">

        <div class="table-c">


            <div class="tcontent">

	            <h1>404  </h1>
	            <p class="sub-title"></p>
	            <a class="button" href="{{url('/')}}" title="">Back to Homepage</a>
                
            </div>

            
        </div>

        
        
        
    </div><!--  container -->

@endsection