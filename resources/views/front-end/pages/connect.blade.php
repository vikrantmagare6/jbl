



        @extends('front-end.layouts.app')
@section('htmlheader_title')
    Connect
@endsection

@section('main-content') 

    <div class="heading-container">
       <h3 class="common-heading">connect</h3>
    </div>

    <section>
        <div class="tab-wrapper">
            <div class="container">
                <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs nav-tabs-responsive" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#featured-alumni" id="featured-alumni-tab" role="tab" data-toggle="tab" aria-controls="featured-alumni" aria-expanded="true">
                                <span class="text">FEATURED ALUMNI</span>
                            </a>
                        </li>
                        <li role="presentation" class="next">
                            <a href="#merchandise" role="tab" id="merchandise-tab" data-toggle="tab" aria-controls="merchandise">
                                <span class="text"> MERCHANDISE</span>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#projects" role="tab" id="projects-tab" data-toggle="tab" aria-controls="projects">
                                <span class="text"> PROJECTS</span>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#alumni-directory" role="tab" id="alumni-directory-tab" data-toggle="tab" aria-controls="alumni-directory">
                                <span class="text">ALUMNI DIRECTORY</span>
                            </a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="featured-alumni" aria-labelledby="featured-alumni-tab">
                            <p class="text-center" style="margin-top: 10px;">Great to know what j.b. women all over the world are upto!!! this space features well-known ex j. b. ites and their achievements.</p>

                            @foreach($featuredalumni as $featuredalumni)
                            <div class="alumni-structure">
                                <img src="{{ url('/public/uploads/images/')}}/{{ $featuredalumni->fa_image_url }}">
                                <h3 class="name">{{ $featuredalumni->fa_name }}</h3>
                                <h4 class="batch">{{ $featuredalumni->fa_designation }}</h4>
                                <div class="line"></div>
                                {!! $featuredalumni->fa_content !!}
                            </div>
                             
                            @endforeach

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="merchandise" aria-labelledby="merchandise-tab">
                            <div class="row">

                            @foreach($merchandise as $merchandise)

                            <div class="col-sm-4">
                                    <div class="merchandise-panel">
                                        <img src="{{url('public/uploads/images')}}/{{$merchandise->m_image1}}" class="img-responsive" />
                                        <?php if ($merchandise->m_image2 != 'default.png'): ?>
                                            <img src="{{url('public/uploads/images')}}/{{$merchandise->m_image2}}" class="img-responsive" />
                                        <?php endif ?>
                                        <h4>{{$merchandise->m_name}} - Rs. {{$merchandise->m_price}}</h4>
                                    </div>
                                </div>

                            @endforeach
                               
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="projects" aria-labelledby="projects-tab">
                            <p style="margin: 20px 0;">Since its inception in 2010, the JBPAA has executed several projects. Money raised by the Managing Committee has been used to fund these projects, each of which has a specific goal. Helping the school with its infrastructure, connecting Alumni, creating a database, and re-launching our website are some of the recent projects that have been undertaken.</p>
                            <p style="margin: 20px 0;">The success of these projects is primarily due to the dedication of the committee and the generosity of our members. Our heartfelt thanks to our supporters for their hard work and the wonderful donations they regularly assist us with. We encourage members to get involved and assist the school and the association with its future projects.</p>
                            <div class="tab-wrapper">
                                <div class="container">
                                    <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                                        <ul id="myTab" class="nav nav-tabs nav-tabs-responsive" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#current-projects" id="current-projects-tab" role="tab" data-toggle="tab" aria-controls="current-projects" aria-expanded="true" class="project-li">
                                                    <span class="text">CURRENT PROJECTS</span>
                                                </a>
                                            </li>
                                            <li role="presentation" class="next">
                                                <a href="#completed-projects" role="tab" id="completed-projects-tab" data-toggle="tab" aria-controls="completed-projects" class="project-li">
                                                    <span class="text">COMPLETED PROJECTS</span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div id="myTabContent" class="tab-content">
                                            <div role="tabpanel" class="pro" id="current-projects" aria-labelledby="current-projects-tab" style="display: block;">

                                            @foreach($project as $project)

                                            <?php 
                                            $dt = Carbon\Carbon::parse($project->p_start_date); ?>

                                            
                                                <div class="row">
                                                    <div class="reunion-gall col-xs-12 col-sm-3 gallery-anchor project-anchor">
                                                        <a href="{{ url('/public/uploads/images') }}/{{$project->p_feature_image}}">
                                                            <img src="{{ url('/public/uploads/images') }}/{{$project->p_feature_image}}" class="img-responsive">
                                                            <div class="overlay">
                                                                <p><span>view album</span><span>(class of <?php                
                                                             echo $dt->year;
                                                             ?>)</span></p>
                                                            </div>
                                                        </a>

                                                        @if(isset($project) && count($project->imagesfile()) > 0)
                                                        @foreach( $project->imagesfile as $img )

                                                        <a href="{{url('/public/uploads/allimg')}}/{{$img->images_url}}">
                                                            <img src="{{url('/public/uploads/allimg')}}/{{$img->images_url}}" class="img-responsive" style="display: none;">
                                                        </a>

                                                        @endforeach
                                                        @endif


                                                        
                                                       
                                                    </div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="project-div">
                                                            <h3 class="name">{{ $project->p_name}}</h3>
                                                            <h4 class="batch"> <?php                
                                                             echo $dt->year;
                                                             ?> </h4>
                                                            <div class="line"></div>
                                                            {!! $project->p_content !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach



                                            </div>
                                            <div role="tabpanel" class="pro" id="completed-projects" aria-labelledby="completed-projects-tab" style="display: none;">


                                            @foreach($project_current as $project_current)

                                            <?php 
                                            $dt = Carbon\Carbon::parse($project_current->p_start_date); ?>

                                            
                                                <div class="row">
                                                    <div class="reunion-gall col-xs-12 col-sm-3 gallery-anchor project-anchor currentacha">
                                                        <a href="{{ url('/public/uploads/images') }}/{{$project_current->p_feature_image}}">
                                                            <img src="{{ url('/public/uploads/images') }}/{{$project_current->p_feature_image}}" class="img-responsive">
                                                            <div class="overlay">
                                                                <p><span>view album</span><span>(class of <?php                
                                                             echo $dt->year;
                                                             ?>)</span></p>
                                                            </div>
                                                        </a>

                                                        @if(isset($project) && count($project_current->imagesfile()) > 0)
                                                        @foreach( $project_current->imagesfile as $img )

                                                        <a href="{{url('/public/uploads/allimg')}}/{{$img->images_url}}">
                                                            <img src="{{url('/public/uploads/allimg')}}/{{$img->images_url}}" class="img-responsive" style="display: none;">
                                                        </a>

                                                        @endforeach
                                                        @endif


                                                        
                                                       
                                                    </div>
                                                    <div class="col-xs-12 col-sm-9">
                                                        <div class="project-div">
                                                            <h3 class="name">{{ $project_current->p_name}}</h3>
                                                            <h4 class="batch"> <?php                
                                                             echo $dt->year;
                                                             ?> </h4>
                                                            <div class="line"></div>
                                                            {!! $project_current->p_content !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach


                                                



                                                   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="alumni-directory" aria-labelledby="alumni-directory-tab">
                            <ul class="alumni-menu">
                                <li class="alumni-active"><a rel="one">FOUNDER MEMBERS</a></li>
                                <li><a rel="two">HONORARY MEMBER</a></li>
                                <li><a rel="three">SILVER MEMBERS</a></li>
                                <li><a rel="four">NORMAL MEMBERS</a></li>
                                <li><a rel="five">TEACHER MEMBERS</a></li>
                            </ul>
                            <!-- FOUNDER MEMBERS starts -->

                            <section class="member-list one" id="one">
                            <h3>Founding Members</h3>
                            <div class="divTable">
                                <div class="divTableBody">
                                    <div class="divTablesRow">

                                    @foreach($founder as $founder)
                                        <div class="divTableCell">
                                            <article class="members-block">
                                                <h5>{{$founder->a_name}}</h5>
                                                <p><i class="fa fa-calendar" aria-hidden="true"></i><span>{{$founder->a_year}}</span></p>
                                                <p>{{$founder->a_email}}</p>
                                            </article>
                                        </div>
                                    @endforeach
                                        
                                       
                                    </div>
                                </div>
                            </div>
                            </section>

                            <!-- FOUNDER MEMBERS ends -->

                            <!-- HONORARY MEMBER starts -->

                            <section class="member-list two" id="two">
                            <h3>HONORARY MEMBER</h3>
                            <div class="divTable">
                                <div class="divTableBody">
                                    <div class="divTablesRow">

                                     @foreach($honorary as $honorary)
                                        <div class="divTableCell">
                                            <article class="members-block">
                                                <h5>{{$honorary->a_name}}</h5>
                                                <p><i class="fa fa-calendar" aria-hidden="true"></i><span>{{$honorary->a_year}}</span></p>
                                                <p>{{$honorary->a_email}}</p>
                                            </article>
                                        </div>
                                    @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                            </section>

                            <!-- HONORARY MEMBER ends -->

                            <!-- SILVER MEMBERS starts -->

                            <section class="member-list three" id="three">
                            <h3>SILVER MEMBERS</h3>
                            <div class="divTable">
                                <div class="divTableBody">
                                    <div class="divTablesRow">

                                    @foreach($silver as $silver)
                                        <div class="divTableCell">
                                            <article class="members-block">
                                                <h5>{{$silver->a_name}}</h5>
                                                <p><i class="fa fa-calendar" aria-hidden="true"></i><span>{{$silver->a_year}}</span></p>
                                                <p>{{$silver->a_email}}</p>
                                            </article>
                                        </div>
                                    @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                            </section>

                            <!-- SILVER MEMBERS ends -->

                            <!-- NORMAL MEMBERS starts -->

                            <section class="member-list four" id="four">
                            <h3>NORMAL MEMBERS</h3>
                            <div class="divTable">
                                <div class="divTableBody">
                                    <div class="divTablesRow">

                                     @foreach($normal as $normal)
                                        <div class="divTableCell">
                                            <article class="members-block">
                                                <h5>{{$normal->a_name}}</h5>
                                                <p><i class="fa fa-calendar" aria-hidden="true"></i><span>{{$normal->a_year}}</span></p>
                                                <p>{{$normal->a_email}}</p>
                                            </article>
                                        </div>
                                    @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                            </section>

                            <!-- NORMAL MEMBERS ends -->

                            <!-- TEACHER MEMBERS starts -->

                            <section class="member-list five" id="five">
                            <h3>TEACHER MEMBERS</h3>
                            <div class="divTable">
                                <div class="divTableBody">
                                    <div class="divTablesRow">
                                     @foreach($teacher as $teacher)
                                        <div class="divTableCell">
                                            <article class="members-block">
                                                <h5>{{$teacher->a_name}}</h5>
                                                <p><i class="fa fa-calendar" aria-hidden="true"></i><span>{{$teacher->a_year}}</span></p>
                                                <p>{{$teacher->a_email}}</p>
                                            </article>
                                        </div>
                                    @endforeach
                                       
                                    </div>
                                </div>
                            </div>
                            </section>

                            <!-- TEACHER MEMBERS ends -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('page_js')

 <script>
       $(".project-li").on("click", function(){
            var id = $(this).attr('aria-controls');
            $(".pro").hide();
            $("#"+id).show();
        });


        $(".alumni-menu li").on("click", function(){
            // var windowscroll = $(window).scrollTop();
            var classname = $(this).find("a").attr("rel");
            var scroll = $("."+classname).offset().top;
            console.log(scroll);
            var $last_value = scroll - 120;
            $("html,body").animate({
                scrollTop : $last_value
            }, 1000);
            $(".alumni-menu li").removeClass("alumni-active");
            $(this).addClass('alumni-active');

       });

       $(window).on("scroll", function(){
            var windowscroll = $(window).scrollTop();
            var alumni_menu_top = $(".common-heading").offset().top;
            if(windowscroll >= alumni_menu_top){
                $(".alumni-menu").addClass("alumni-menu-stick");
            }
            else{
                $(".alumni-menu").removeClass("alumni-menu-stick");
            }
            $('.member-list').each(function() {                
                var scroll = $(this).offset().top;
                var ls_scroll = scroll + 200;
                if ($(window).scrollTop() + $(window).height() >= ls_scroll) {
                    var id = $(this).attr('id');
                    console.log(id);
                    $('.alumni-menu li').removeClass('alumni-active');
                    $(".alumni-menu li a[rel="+id+"]").parent().addClass('alumni-active');
                }
            });
       });
   </script>



@endsection