@extends('front-end.layouts.app')
@section('htmlheader_title')
    Gallery
@endsection

@section('main-content') 

    <div class="container gallery-top-bottom">
            <div class="heading-container">
               <h3 class="common-heading">gallery</h3>
            </div>
            <div class="row">
                <div class="gallery">

                @foreach($imggal as $gal)


                <a href="{{ url('public/uploads/images')}}/{{$gal->gallery_image}}" class="col-xs-12 col-sm-3 gallery-anchor">
                    <img src="{{ url('public/uploads/images')}}/{{$gal->gallery_image}}" class="img-responsive" />
                    <div class="overlay">
                        <p><span>click to view</span><span></span></p>
                    </div>
                </a>
                


                @endforeach




                    


                    


                </div>
            </div>
        </div>

@endsection


@section('page_js')
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>
 <script>
    $('.gallery').lightGallery();
    $('.gallery').masonry({
        itemSelector:".gallery-anchor",
        columnWidth:".gallery-anchor",
        percentPosition:!0
    });
    </script>
@endsection