<!DOCTYPE html>
<html>
<head>
	<title>Home Page</title>
	 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
</head>
<body>

<a href="/">HOME</a>

<h1>Student List form</h1>

<form enctype="multipart/form-data" method="POST" action="{{ url('re-union') }}">

				{!! csrf_field() !!}
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="std_name" placeholder="Name">
                </div> 
                <div class="col-md-6">
                  <input type="email" class="form-control margin-bottom-30" name="std_email" placeholder="Email Address">
                </div> 
                <div class="col-md-6">
                  <input type="text" class="form-control margin-bottom-30" name="std_year_of_pass" placeholder="Year of passing">
                </div> 
                 
                <div class="col-md-12">
                    <label class="control-label">Reunion Gallery</label>
                    <input id="std_gallery" name="std_gallery[]" type="file" multiple class="file-loading">
                </div>
                <div class="col-md-12">
                  <div class="captcha-block">
                    <div class="g-recaptcha" data-sitekey="6LfuFTIUAAAAAOlDoSj1ZWvYH74XPAPY3bqUkfl6"></div>
                    <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                  </div> <!-- captcha-block -->
                </div>
                <div class="col-md-12 margin-top-30">
                  <input type="submit" class="btn  btn-primary btn-lg" value="Submit">
                </div>    
	
</form>





</body>
</html>