@extends('front-end.layouts.app')
@section('htmlheader_title')
	About Us
@endsection

@section('main-content') 

	<div class="heading-container">
       <h3 class="common-heading">about us</h3>
    </div>

    <section>
        <div class="tab-wrapper">
            <div class="container">
                <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs nav-tabs-responsive" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#history" id="history-tab" role="tab" data-toggle="tab" aria-controls="history" aria-expanded="true">
                                <span class="text">HISTORY</span>
                            </a>
                        </li>
                        <li role="presentation" class="next">
                            <a href="#principal" role="tab" id="principal-tab" data-toggle="tab" aria-controls="principal">
                                <span class="text">PRINCIPAL'S ADDRESS</span>
                            </a>
                        </li>
                        <!-- <li role="presentation" class="dropdown">
                            <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents">
                                <span class="text">Dropdown</span>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                                <li>
                                    <a href="#dropdown1" tabindex="-1" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1">
                                        <span>1</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#dropdown2" tabindex="-1" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2">
                                        <span>2</span>
                                    </a>
                                </li>
                            </ul>
                        </li> -->
                        <li role="presentation">
                            <a href="#managing-commitee" role="tab" id="managing-commitee-tab" data-toggle="tab" aria-controls="managing-commitee">
                                <span class="text">MANAGING COMMITTEE</span>
                            </a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="history" aria-labelledby="history-tab">
                            
                            <div class="full-table">
                                <div class="table-row">
                                <?php 

                                 ?>
                                    <div class="table-cell history-image" style="background-image: url({{ url('public/uploads/images/') }}/{{$about_history_image->option_value }})">
                                    </div>


                                    {!! $about_history_content->option_value !!}




                                    


                                </div>
                            </div>


                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="principal" aria-labelledby="principal-tab">
                            
                          <div class="full-table">
                              <div class="table-row">
                                  <div class="table-cell principal-image" style="background-image: url({{ url('public/uploads/images/') }}/{{$about_principal_image->option_value}})">
                                  </div>
                                  <div class="table-cell principal-content">
                                      <div class="principal-info">
                                          
                                          <h2>{{$about_principal_title->option_value}}</h2>
                                          <h3>{{$about_principal_subtitle->option_value}}</h3>
                                      </div>
                                      {!! $about_principal_content->option_value !!}
                                  </div>
                              </div>
                          </div>


                        </div>
                        <!-- <div role="tabpanel" class="tab-pane fade" id="dropdown1" aria-labelledby="dropdown1-tab">
                            <p>sumit</p>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="dropdown2" aria-labelledby="dropdown2-tab">
                            <p>purnima</p>
                        </div> -->
                        <div role="tabpanel" class="tab-pane fade" id="managing-commitee" aria-labelledby="managing-commitee-tab">
                              <!-- managing-commitee code starts -->

                              @foreach($corecommittee as $committee)

                              <div class="row mc-article">
                                <div class="col-sm-4 col-md-3">
                                    <div class="mc-image">
                                        <img src="{{ url ('public/uploads/images')}}/{{ $committee->committee_img}}" class="img-responsive" alt="Naheed Divecha">
                                    </div>
                                </div>
                                <div class="col-sm-8 col-md-9">
                                    <article class="mc-content">
                                        <h3 class="name">{{ $committee->committee_name }}</h3>
                                        <h4 class="designation">{{ $committee->committee_designation}}</h4>
                                        <div class="line"></div>
                                        {!! $committee->committee_description !!}
                                    </article>
                                </div>
                            </div>


                            @endforeach



                              


                              


                            


                           
                            <!-- managing-commitee code ends -->
                            <div class="seperator-line"></div>
                            <!-- advisory commitee code starts -->
                            <h3 class="advisory-header">ADVISORY COMMITTEE</h3>

                            @foreach($advisorycommittee as $committee)

                            <div class="row mc-article">
                                <div class="col-sm-4 col-md-3">
                                    <div class="mc-image">
                                        <img src="{{ url ('public/uploads/images')}}/{{ $committee->committee_img}}" class="img-responsive" alt="Jeroo-Buhariwala">
                                    </div>
                                </div>
                                <div class="col-sm-8 col-md-9">
                                    <article class="mc-content">
                                        <h3 class="name">{{ $committee->committee_name }}</h3>
                                        <h4 class="designation">{{ $committee->committee_designation}}</h4>
                                        <div class="line"></div>
                                        {!! $committee->committee_description !!}
                                    </article>
                                </div>
                            </div>


                            @endforeach
                            
                            <!-- advisory commitee code ends -->
                            <div class="seperator-line"></div>
                            <!-- founder members code starts -->

                            <h3 class="advisory-header">FOUNDER MEMBERS</h3>                           
                            


                            @foreach($foundercommittee as $committee)

                            <div class="row mc-article">
                                <div class="col-sm-4 col-md-3">
                                    <div class="mc-image">
                                        <img src="{{ url ('public/uploads/images')}}/{{ $committee->committee_img}}" class="img-responsive" alt="Jeroo-Buhariwala">
                                    </div>
                                </div>
                                <div class="col-sm-8 col-md-9">
                                    <article class="mc-content">
                                        <h3 class="name">{{ $committee->committee_name }}</h3>
                                        <h4 class="designation">{{ $committee->committee_designation}}</h4>
                                        <div class="line"></div>
                                        {!! $committee->committee_description !!}
                                    </article>
                                </div>
                            </div>


                              @endforeach
                            <!-- founder members code ends -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection