



        @extends('front-end.layouts.app')
@section('htmlheader_title')
    Home
@endsection

@section('main-content') 

<style type="text/css">
  .grid-item{width:33.33%;padding:0 10px 10px 0}@media(max-width:991px){.grid-item{width:50%;padding:0 10px 10px 0}}@media(max-width:500px){.grid-item{width:100%;padding:0 0 10px 0}}@media(max-width:991px){body{margin-top:95px}.heading-container{margin:40px 0 10px 0}}
</style>

        <div class="container events-inner-container">
          <!-- thumnail with date starts -->
          <div class="alumni-structure">
              <img src="{{ URL::asset('public/uploads/images/'.$event->event_large_img) }}">
              <h3 class="name">{{$event->event_name}}</h3>
              <h4 class="batch">CLASS OF '94, JASMINE HOUSE CAPTAIN</h4>
              <div class="line"></div>
              {!! $event->event_content !!}
          </div>
          <!-- masonary starts here --> 

          <div class="heading-container event-heading">
             <h3 class="common-heading">gallery</h3>
          </div>
          <div class="grid">


             @forelse($event->imagesfile as $img)

              

                <div class="grid-item"><img src="{{ URL::asset('public/uploads/allimg/'.$img->images_url) }}" class="img-responsive"></div>


            @empty
            <p>No data found</p>
            @endforelse




            
          </div>
          <!-- masonary ends here -->
        </div>

@endsection

@section('page_js')
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>
<script type="text/javascript">

  $(window).load(function() {
  $('.grid').masonry({
        itemSelector:".grid-item",
        columnWidth:".grid-item",
        percentPosition:!0

    });
});
  
</script>
@endsection